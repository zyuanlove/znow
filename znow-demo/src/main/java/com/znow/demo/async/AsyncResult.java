package com.znow.demo.async;

import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AsyncResult {

    private Byte[] resultByte;

    private AtomicBoolean isDone = new AtomicBoolean(false);

    private Lock lock = new ReentrantLock();

    private Condition condition;

    private long startTime;

    public AsyncResult() {
        condition = lock.newCondition();
        startTime = System.currentTimeMillis();
    }

    public Byte[] get() {
        lock.lock();
        try {
            if (!isDone.get()) {
                condition.await();
            }
        } catch (InterruptedException e) {

        } finally {
            lock.unlock();
        }
        return null;
    }

    public boolean getIsDone() {
        return isDone.get();
    }

    public Byte[] get(long timeOut, TimeUnit unit) {
        lock.lock();
        try {
            boolean flag = true;
            if (!isDone.get()) {
                long overAllTimeout = timeOut - (System.currentTimeMillis() - startTime);
                if (overAllTimeout > 0) {
                    // 设置等待超时的时间
                    flag = condition.await(overAllTimeout, TimeUnit.MILLISECONDS);
                } else {
                    flag = false;
                }
            }

            if (!flag && !isDone.get()) {
                throw new RuntimeException("超时异常");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
        return resultByte;
    }

    //public void result

    public static void main(String[] args) {
    }
}
