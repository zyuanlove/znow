package com.znow.demo.collection.list;

/**
 * 链表
 */
public class SingleLinedList {

    private int size;

    private Node head;

    public SingleLinedList() {
        size = 0;
        head = null;
    }

    public int size() {
        return this.size;
    }

    /**
     * 从表头添加元素
     */
    public void addHead(Object data) {
        Node newNode = new Node(data);
        if (size == 0) {
            head = newNode;
        } else {
            // 原来的头结点变为新结点的下一个结点
            newNode.next = head;
            // 原来的头结点赋值为新加的数据
            head = newNode;
        }
        size++;
    }

    public Object find(Object obj) {
        Node firsthead = head;
        int tempSize = size;
        while (tempSize > 0) {
            if (obj.equals(head)) {
                return firsthead;
            } else {
                firsthead = firsthead.next;
            }
            tempSize--;
        }
        return null;
    }

    public void print() {
        Node firstHead = head;
        int tempSize = size;
        if (tempSize == 1) {
            System.out.println(firstHead.data);
            return;
        }
        while (tempSize > 0) {
            if (firstHead.equals(head)) {
                System.out.println(firstHead.data);
            } else if (firstHead.next == null) {
                System.out.println(firstHead.data);
            } else {
                System.out.println(firstHead.data);
            }
            firstHead = firstHead.next;
            tempSize--;

        }
    }

    private class Node {
        /**
         * 存储当前节点数据域
         */
        private Object data;

        /**
         * 存储下一个结点指针域
         */
        private Node next;

        public Node(Object data) {
            this.data = data;
        }

        public Node(Object data, Node next) {
            this.data = data;
            this.next = next;
        }
    }

    public static void main(String[] args) {
        SingleLinedList singleLinedList = new SingleLinedList();
        singleLinedList.addHead(1);
        singleLinedList.addHead(2);
        singleLinedList.addHead(3);
        singleLinedList.addHead(4);
        singleLinedList.addHead(5);
        /*singleLinedList.addNext(6);
        singleLinedList.addNext(7);
        singleLinedList.addNext(8);
        singleLinedList.addNext(9);*/

        singleLinedList.print();

    }
}
