package com.znow.demo.collection.list;

/**
 * 栈 先进后出 数据结构
 */
public class MyStack {

    private int size;

    private Object[] item;

    /**
     * 栈顶指针 默认-1
     */
    private int top;

    public MyStack() {
        top = -1;
        item = new Object[10];
    }

    /**
     * 返回栈的大小
     */
    public int size() {
        return top + 1;
    }

    public boolean isEmpty() {
        return top < 0;
    }

    public void push(Object data) {
        if (size() > item.length) {
            // 扩容
        }
        item[++top] = data;
    }

    public void pop() {
        item[top--] = null;
    }

    /**
     * 返回栈顶元素
     */
    public Object peek() {
        return item[top];
    }

    public static void main(String[] args) {
        MyStack myStack = new MyStack();
        myStack.push(1);
        myStack.push(2);
        myStack.push(3);


        for (int i = 0; i < myStack.size(); i++) {
            System.out.println(myStack.item[i]);
        }


    }
}
