package com.znow.demo.collection.list;


import java.util.LinkedList;

public class MyLinedList {

    private int size;

    /**
     * 链表头
     */
    private Node head;

    /**
     * 链表尾
     */
    private Node last;

    /**
     * 从头插入元素
     */
    public void addHead(Object data) {
        Node newNode = new Node(data);
        newNode.next = head;

        if (head == null) {
            head = newNode;
            last = newNode;
        } else {
            head.prev = newNode;
            head = newNode;
        }
        size++;
    }

    /**
     * 从尾部插入元素
     */
    public void addLast(Object data) {
        Node newNode = new Node(data);
        newNode.prev = last;

        if (last == null) {
            last = newNode;
            head = newNode;
        } else {
            last.next = newNode;
            last = newNode;
        }
        size++;
    }

    public class Node {

        /**
         * 元素
         */
        private Object item;

        /**
         * 下一个结点
         */
        private Node next;

        /**
         * 上一个结点
         */
        private Node prev;

        public Node(Object item) {
            this.item = item;
        }
    }

    public static void main(String[] args) {
        MyLinedList myLinedList = new MyLinedList();
        myLinedList.addLast(1);
        myLinedList.addLast(2);
        myLinedList.addLast(3);
        myLinedList.addLast(4);
        myLinedList.addLast(5);

        myLinedList.addHead(6);
        myLinedList.addHead(7);
        myLinedList.addHead(8);

        StringBuilder headToString = new StringBuilder();
        StringBuilder lastToString = new StringBuilder();
        // 链表大小
        int tempSize = myLinedList.size;
        // 链表尾结点
        Node last = myLinedList.last;
        // 链表头结点
        Node head = myLinedList.head;

        while (tempSize > 0) {
            // 拼装链表字符串
            headToString.append(head.item).append("->");
            // 拼装链表字符串
            lastToString.append(last.item).append("->");

            head = head.next;
            last = last.prev;
            tempSize--;
        }

        System.out.println(headToString.toString());
        System.out.println(lastToString.toString());
    }
}
