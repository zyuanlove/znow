package com.znow.demo.collection.list;

import java.util.Iterator;

/**
 * 自定义动态数组
 */
public class MyArrayList implements Iterable{

    private Object[] elementData;

    /**
     * 数组大小
     */
    private int size;

    public MyArrayList() {
        this.size = 0;
        elementData = new Object[10];
    }

    public int getSize() {
        return this.size;
    }

    public void add(Object data) {
        add(getSize(), data);
    }

    public void add(int index, Object data) {
        if (getSize() > elementData.length) {
            expendSpace();
        }
        for (int i = getSize(); i > index; i--) {
            elementData[i] = elementData[i - 1];
        }
        elementData[index] = data;
        size++;
    }

    public Object remove(int index) {
        for (int i = index; i < getSize() - 1; i++) {
            elementData[i] = elementData[i + 1];
        }
        elementData[size--] = null;
        return get(index);
    }

    public Object get(int index) {
        return elementData[index];
    }

    /**
     * 扩充容量；把当前数组大小*2，赋值给原来数组
     */
    public void expendSpace() {
        Object[] temp = new Object[elementData.length * 2];
        for (int i = 0; i < elementData.length; i++) {
            temp[i] = elementData[i];
        }
        elementData = temp;
    }

    @Override
    public Iterator iterator() {
        return new Ite(this);
    }

    /**
     * 根据索引位置删除元素
     */
    private class Ite<T> implements Iterator {

        private int pre;

        private MyArrayList theList;

        private Ite(MyArrayList list) {
            theList = list;
        }

        @Override
        public boolean hasNext() {
            return pre < getSize();
        }

        @Override
        public Object next() {
            return theList.elementData[pre++];
        }
    }

    public static void main(String[] args) {
        MyArrayList myArrayList = new MyArrayList();
        myArrayList.add(123);
        myArrayList.add(123);
        myArrayList.add(123);

        for (int i = 0; i < myArrayList.elementData.length; i++) {
            System.out.println(myArrayList.get(i));
        }
    }
}
