package com.znow.demo.math;

public class Gift {

    private int index;

    private String giftId;

    private String giftName;

    private Double probability;

    public Gift(int index, String giftId, String giftName, Double probability) {
        this.index = index;
        this.giftId = giftId;
        this.giftName = giftName;
        this.probability = probability;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getGiftId() {
        return giftId;
    }

    public void setGiftId(String giftId) {
        this.giftId = giftId;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public Double getProbability() {
        return probability;
    }

    public void setProbability(Double probability) {
        this.probability = probability;
    }

    @Override
    public String toString() {
        return "Gift{" +
                "index=" + index +
                ", giftId='" + giftId + '\'' +
                ", giftName='" + giftName + '\'' +
                ", probability=" + probability +
                '}';
    }
}
