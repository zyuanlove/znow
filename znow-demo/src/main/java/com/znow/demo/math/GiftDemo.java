package com.znow.demo.math;

import com.google.common.collect.Lists;

import java.util.*;

public class GiftDemo {

    public static void main(String[] args) {
       /* List<com.znow.demo.math.utils.Gift> giftList = Lists.newArrayList();
        giftList.add(new com.znow.demo.math.utils.Gift(1,"1","iphone",0.1D));
        giftList.add(new com.znow.demo.math.utils.Gift(2,"2","鼠标",0.2D));
        giftList.add(new com.znow.demo.math.utils.Gift(3,"3","优惠券",0.25D));
        giftList.add(new com.znow.demo.math.utils.Gift(4,"4","谢谢惠顾",0.55D));
        giftList.add(new com.znow.demo.math.utils.Gift(5,"5","无",0.25D));

        //
        List<Double> orignalRates = Lists.newArrayList();

        for (com.znow.demo.math.utils.Gift gift : giftList) {
            Double probability = gift.getProbability();
            orignalRates.add(probability);
        }

        // statistics
        Map<Integer, Integer> count = new HashMap<Integer, Integer>();
        double num = 1000000;
        for (int i = 0; i < num; i++) {
            int orignalIndex = lottery(orignalRates);

            Integer value = count.get(orignalIndex);
            count.put(orignalIndex, value == null ? 1 : value + 1);
        }

        for (Map.Entry<Integer, Integer> entry : count.entrySet()) {
            System.out.println(giftList.get(entry.getKey()) + ", count=" + entry.getValue() + ", probability="
                    + entry.getValue() / num);
        }*/



    }

    /**
     * 抽奖
     *
     * @param orignalRates 原始的概率列表，保证顺序和实际物品对应
     * @return 物品的索引
     */
    public static int lottery(List<Double> orignalRates){
        if(orignalRates == null || orignalRates.isEmpty()){
            return -1;
        }

        int size = orignalRates.size();

        // 计算总概率，这样可以保证不一定总概率是1
        double sumRate = 0d;
        for (double rate : orignalRates) {
            sumRate += rate;
        }

        // 计算每个物品在总概率的基础下的概率情况
        List<Double> sortOrignalRates = new ArrayList<Double>(size);
        Double tempSumRate = 0d;
        for (double rate : orignalRates) {
            tempSumRate += rate;
            sortOrignalRates.add(tempSumRate / sumRate);
        }

        // 根据区块值来获取抽取到的物品索引
        double nextDouble = Math.random();
        sortOrignalRates.add(nextDouble);
        Collections.sort(sortOrignalRates);
        return sortOrignalRates.indexOf(nextDouble);
    }

}
