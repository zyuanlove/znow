package com.znow.demo.threads.accout;

/**
 * 如何规避死锁？
 * 1：互斥
 * 2：占有且等待 （一次性申请所有资源抽象一层管理员来做资源控制）
 * 3：不可抢占条件
 * 4：循环等待
 */
public class Account {

    /**
     * 账户余额
     */
    private int balance;

    // 应该是单例的
    private AccountManager accountManager;

    /**
     * 破坏占有且等待转账业务逻辑
     * @param target 转账给谁
     * @param amount 金额
     */
    public void transfer(Account target,int amount){
        // 破坏占有且等待条件
        while (!accountManager.apply(this,target)){
            try {
                // 先锁定转出账户
                synchronized (this){
                    if (this.balance > amount){
                        this.balance -= amount;
                        target.balance += amount;
                    }
                }
            }finally {
                // 成功释放资源
                accountManager.free(this,target);
            }

        }
    }
}
