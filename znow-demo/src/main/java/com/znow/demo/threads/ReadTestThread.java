package com.znow.demo.threads;

import lombok.Synchronized;

import java.util.concurrent.locks.ReentrantLock;

public class ReadTestThread implements Runnable {

    private ReentrantLock lock = new ReentrantLock(false);

    // 共享变量
    Integer abc = 1000;

    @Override
    public void run() {
        //System.out.println("当前线程:" + Thread.currentThread().getName());

        for (int i = 0; i < 1000; i++) {
            lock.lock();
            try {
                abc--;
                System.out.println(abc);

                Thread.sleep(1000L);
            }catch (Exception e){
            }finally {
                lock.unlock();
            }
        }

    }

}
