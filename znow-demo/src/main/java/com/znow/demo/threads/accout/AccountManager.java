package com.znow.demo.threads.accout;

import java.util.ArrayList;
import java.util.List;

/**
 * 破坏占有且等待条件资源管理者
 */
public class AccountManager {

    private List<Object> resourceList = new ArrayList<>(2);

    /**
     * 向账户管理者申请资源
     */
    public synchronized boolean apply(Object from, Object to){
        if(resourceList.contains(from) || resourceList.contains(to)){
            return false;
        }else{
            resourceList.add(from);
            resourceList.add(to);
        }
        return true;
    }

    /**
     * 向账户管理者归还资源
     */
    public synchronized void free(Object from, Object to){
        resourceList.remove(from);
        resourceList.remove(to);

    }

    /**
     * 等待通知
     * 向账户管理者申请资源
     */
    public synchronized void waitApply(Object from, Object to){
        while (resourceList.contains(from) || resourceList.contains(to)){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        resourceList.add(from);
        resourceList.add(to);
    }

    /**
     * 等待通知
     * 向账户管理者归还资源
     */
    public synchronized void waitFree(Object from, Object to){
        resourceList.remove(from);
        resourceList.remove(to);
        notifyAll();
    }
}
