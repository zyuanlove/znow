package com.znow.demo.socket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketServerDemo {

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(9000);

        while (true){
            System.out.println("接受客户端请求");

            Socket accept = serverSocket.accept();

            InputStream inputStream = accept.getInputStream();

            byte[] bytes = new byte[1024];
            int len = -1;
            len = inputStream.read(bytes);

            String getData = new String(bytes, 0, len);
            System.out.println("从客户端接受的数据：" + getData);


            inputStream.close();
            accept.close();
        }
    }
}
