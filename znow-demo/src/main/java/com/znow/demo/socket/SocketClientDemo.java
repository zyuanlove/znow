package com.znow.demo.socket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class SocketClientDemo {

    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(System.in);

        System.out.println("请输入数据：");

        String inputDate = input.nextLine();

        Socket socket = new Socket("127.0.0.1",9000);

        OutputStream outputStream = socket.getOutputStream();
        outputStream.write(inputDate.getBytes());

        //获取服务端回传的数据
        InputStream is = socket.getInputStream();
        byte[] buffer = new byte[1024];
        int len = -1;
        len = is.read(buffer);
        String getData = new String(buffer, 0, len);
        System.out.println("从服务端获取的数据:" + getData);
        //是否流
        is.close();
        outputStream.close();
        socket.close();

    }
}
