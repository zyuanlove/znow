package com.znow.demo.callback;

public interface ICallback<T, E> {

    T doCallback(E e);

}   