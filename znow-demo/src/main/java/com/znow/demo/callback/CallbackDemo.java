package com.znow.demo.callback;

public class CallbackDemo {

    public void execute(ICallback fun) {
        fun.doCallback(null);
    }

    public static void main(String[] args) {
        CallbackDemo callbackDemo = new CallbackDemo();
        callbackDemo.execute(new ICallback() {
            @Override
            public Object doCallback(Object o) {
                System.out.println("成功");
                return "123";
            }
        });

    }


}
