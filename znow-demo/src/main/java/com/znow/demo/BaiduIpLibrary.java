package com.znow.demo;


import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.List;

/**
 * <h3>Baidu.com</h3>
 *
 * @author allen
 * @since V1.0.0 2017/12/14
 */
public class BaiduIpLibrary {


    public IP find(String ip) {
        try {
            String html = getHtml(ip);
            String[] strings = find(html, ip);
            return new IP("", strings[0], strings[1]);
        } catch (Exception e) {
            return null;
        }
    }

    private static String getHtml(String ip) throws IOException {
        URL url = new URL("http", "www.baidu.com", "/s?wd=" + ip);
        URLConnection connection = url.openConnection();
        connection.connect();
        List<String> lines = IOUtils.readLines(connection.getInputStream(), Charset.defaultCharset());
        return ArrayUtils.toString(lines);
    }

    private static String[] find(String html, String ip) throws Exception {
        Document document = Jsoup.parse(html);
        Element gap = document.select("span.c-gap-right").first().parent();
        String text = gap.text();
        text = StringUtils.substring(text, StringUtils.indexOf(text, ip) + ip.length(), text.length());
        text = StringUtils.split(text, ' ')[0];
        int splitIndex = StringUtils.indexOf(text, "省");
        if (splitIndex == -1) {
            splitIndex = StringUtils.indexOf(text, "市");
        }
        String province = StringUtils.substring(text, 0, splitIndex);
        String city = StringUtils.substring(text, splitIndex + 1, text.length() - 1);
        return new String[]{province, city};
    }

    public static void main(String[] args) {
        String ip = "122.225.200.195";
        BaiduIpLibrary baiduIpLibrary = new BaiduIpLibrary();
        System.out.println(baiduIpLibrary.find(ip));
    }
}