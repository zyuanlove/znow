/*
 * Copyright 2016 bianxianmao.com All right reserved. This software is the confidential and proprietary information of
 * textile.com ("Confidential Information"). You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered into with bianxianmao.com.
 */

package com.znow.demo;


import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * <h3>IP</h3>
 *
 * @author allen
 * @since V1.0.0 2017/12/14
 */
public class IP {

    /**
     * 国家
     */
    private String country;
    /**
     * 省份或直辖市
     */
    private String province;
    /**
     * 城市或地区
     */
    private String city;
    /**
     * 学校/单位/机关/部门
     */
    private String division;
    /**
     * 运营商
     */
    private String telecom;
    /**
     * 经度
     */
    private String longitude;
    /**
     * 纬度
     */
    private String latitude;
    /**
     * 时区 Asia/Shanghai
     */
    private String timezone;
    /**
     * UTC时区 UTC+8
     */
    private String utczone;
    /**
     * 行政区划代码
     */
    private String regioncode;
    /**
     * 国际电话代码
     */
    private int telphonenum;
    /**
     * 国家二位代码
     */
    private String countrycode;

    public IP() {
    }

    public IP(String country, String province, String city) {
        this.country = country;
        this.province = province;
        this.city = city;
    }

    public IP(String country, String province, String city, String regioncode) {
        this.country = country;
        this.province = province;
        this.city = city;
        this.regioncode = regioncode;
    }

    public IP(String country, String province, String city, String division, String telecom, String longitude, String latitude, String timezone, String utczone, String regioncode, int telphonenum, String countrycode) {
        this.country = country;
        this.province = province;
        this.city = city;
        this.division = division;
        this.telecom = telecom;
        this.longitude = longitude;
        this.latitude = latitude;
        this.timezone = timezone;
        this.utczone = utczone;
        this.regioncode = regioncode;
        this.telphonenum = telphonenum;
        this.countrycode = countrycode;
    }

    public static IP createDefault() {
        return new IP("中国", "北京", "北京");
    }

    public boolean isAvailable() {
        return StringUtils.isNotBlank(this.province) && this.province.length() >= 2 && StringUtils.isNotBlank(this.city) && this.city.length() >= 2;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getTelecom() {
        return telecom;
    }

    public void setTelecom(String telecom) {
        this.telecom = telecom;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getUtczone() {
        return utczone;
    }

    public void setUtczone(String utczone) {
        this.utczone = utczone;
    }

    public String getRegioncode() {
        return regioncode;
    }

    public void setRegioncode(String regioncode) {
        this.regioncode = regioncode;
    }

    public int getTelphonenum() {
        return telphonenum;
    }

    public void setTelphonenum(int telphonenum) {
        this.telphonenum = telphonenum;
    }

    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
