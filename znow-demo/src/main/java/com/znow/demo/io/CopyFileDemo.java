package com.znow.demo.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyFileDemo {

    public static void main(String[] args) throws IOException {
        FileInputStream fileInputStream = new FileInputStream("d://nihao.txt");

        File file = new File("d://nihao2.txt");
        if (file.exists()) {
            file.createNewFile();
        }

        FileOutputStream fileOutputStream = new FileOutputStream(file);

        int len = 1024;
        byte[] bytes = new byte[len];

        while (-1 != (len = fileInputStream.read(bytes, 0, len))) {
            fileOutputStream.write(bytes);
        }

        fileOutputStream.close();
        fileInputStream.close();
    }
}
