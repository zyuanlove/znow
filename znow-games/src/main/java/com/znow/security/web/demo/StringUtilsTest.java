package com.znow.security.web.demo;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class StringUtilsTest {

    private static final Map<Character,Character> brackets = new HashMap();

    static {
        brackets.put(')','(');
        brackets.put(']','[');
        brackets.put('}','{');
        brackets.put('>','<');
    }

    public static boolean isMatch(String str){
        if(str == null){
            return false;
        }
        Stack<Character> stack = new Stack<Character>();
        for (char ch : str.toCharArray()){
            if(brackets.containsValue(ch)){
                stack.push(ch);
            }else if(brackets.containsKey(ch)){
                if(stack.empty() || stack.pop() != brackets.get(ch)){
                    return false;
                }
            }
        }
        return stack.empty();
    }

}
