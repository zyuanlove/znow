package com.znow.security.web.demo;

public class LinkedLists {

    private int size = 0;

    private Node header = null;

    public void addNode(Object data){
        Node temp = new Node(data);
        if(size == 0){
            header = temp;
        }else {
            temp.next = header;
            header = temp;
        }
        size++;
    }

    //显示节点信息
    public void display(){
        if(size >0){
            Node node = header;
            int tempSize = size;
            if(tempSize == 1){//当前链表只有一个节点
                System.out.println("["+node.data+"]");
                return;
            }
            while(tempSize>0){
                if(node.equals(header)){
                    System.out.print("["+node.data+"->");
                }else if(node.next == null){
                    System.out.print(node.data+"]");
                }else{
                    System.out.print(node.data+"->");
                }
                node = node.next;
                tempSize--;
            }
            System.out.println();
        }else{//如果链表一个节点都没有，直接打印[]
            System.out.println("[]");
    }

    }

    class Node{

        public Object data;

        public Node next;

        public Node(Object data){
            this.data = data;
        }
    }

    public static void main(String[] args) {
        LinkedLists linkedLists = new LinkedLists();
        linkedLists.addNode(123);
        linkedLists.addNode(4565);
        linkedLists.display();
    }
}
