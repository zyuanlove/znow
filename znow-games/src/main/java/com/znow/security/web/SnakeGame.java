package com.znow.security.web;

import javax.swing.*;
import java.awt.*;

public class SnakeGame {

    private static void init(JFrame jFrame,int width,int heigth){
        jFrame.setTitle("我的贪吃蛇");
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int screenWidth = dimension.width;
        int screenHeigth = dimension.height;
        System.out.println("分辨率===="+ screenHeigth + "====="+screenHeigth);
        int x = (screenWidth - width) / 2;
        int y = (screenHeigth - heigth) / 2;
        // 设置当前窗体出现在窗口中坐标位置,即x轴的坐标值和y轴的坐标值
        jFrame.setLocation(x,y);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setVisible(true);
        jFrame.setSize(width,heigth);

    }

    public static void main(String[] args) {
        // 构建游戏场景
        JFrame jFrame = new JFrame();
        init(jFrame ,800,600);


    }


}
