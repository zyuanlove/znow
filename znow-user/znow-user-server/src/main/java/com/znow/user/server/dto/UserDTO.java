package com.znow.user.server.dto;

import lombok.Data;

import java.util.List;

@Data
public class UserDTO {

    private Integer id;

    private String name;

    private Boolean deleteFlag;

    private List<UserDTO> userData;

    private Object datas;
}
