package com.znow.user.web.config;

import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.znow.user.LoginFilter;
import com.znow.user.web.LoginThreadLocalInterceptor;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ServletComponentScan
public class WebConfiguration implements WebMvcConfigurer {

    @Bean
    public LoginThreadLocalInterceptor loginThreadLocalInterceptor() {
        return new LoginThreadLocalInterceptor();
    }

    /**
     * 添加拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginThreadLocalInterceptor()).addPathPatterns("/**");
    }

    /**
     * 设置过滤器
     */
    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(new LoginFilter());
        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.setName("loginFilter");
        return filterRegistrationBean;
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        // 使用FastJson转JSON
        FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
        FastJsonConfig config = new FastJsonConfig();

        SerializerFeature[] serializerFeatures = {
                // 数字类型为空默认显示0
                SerializerFeature.WriteNullNumberAsZero,
                // 字符串类型为空默认为""
                SerializerFeature.WriteNullStringAsEmpty,
                // boolean类型为空默认为false
                SerializerFeature.WriteNullBooleanAsFalse,
                // 数组类型为空默认为[]
                SerializerFeature.WriteNullListAsEmpty,
                // 格式化json
                SerializerFeature.PrettyFormat,
                // 关闭递归依赖检查
                SerializerFeature.DisableCircularReferenceDetect
        };

        config.setSerializerFeatures(serializerFeatures);
        ParserConfig.getGlobalInstance().setAutoTypeSupport(true);

        List<MediaType> mediaTypes = new ArrayList<>();
        // 解决中文乱码
        mediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
        converter.setSupportedMediaTypes(mediaTypes);
        converter.setFastJsonConfig(config);
        converters.add(converter);
        //converters.add(new JacksonHttpMessageConverter());
    }
}
