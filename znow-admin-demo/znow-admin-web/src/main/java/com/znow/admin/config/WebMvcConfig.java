package com.znow.admin.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * web 环境配置项
 *
 * @author yuan 2018/3/30 15:25
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {


}
