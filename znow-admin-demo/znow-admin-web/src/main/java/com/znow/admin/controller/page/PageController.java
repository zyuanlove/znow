package com.znow.admin.controller.page;

import com.znow.common.utils.UUIDUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 跳转页面
 *
 * @author yuan 2018/12/7 15:43
 */
@Controller
public class PageController {

    @RequestMapping("/")
    public String index(){
        return "views/index";
    }

    @RequestMapping("/index.html")
    public String indexPage(){
        return "views/index";
    }

    @RequestMapping("/home.html")
    public String homePage(){
        return "views/home/homepage";
    }

    @RequestMapping("/login.html")
    public ModelAndView loginPage(){
        ModelAndView modelAndView = new ModelAndView();
        String token = UUIDUtils.getUUID();
        modelAndView.addObject("captchaToken",token);
        modelAndView.setViewName("views/user/login");
        return modelAndView;
    }

}
