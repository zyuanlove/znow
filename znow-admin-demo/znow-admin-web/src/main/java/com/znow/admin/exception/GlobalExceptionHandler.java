package com.znow.admin.exception;

import com.znow.admin.security.exception.LoginParamException;
import com.znow.common.vo.ServerResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(AuthenticationException.class)
    public ServerResponse LoginParamException(LoginParamException e) {
        logger.error("登录参数异常：{}", e.getMessage());
        return ServerResponse.createByErrorCodeMessage(400000, e.getMessage());
    }
}
