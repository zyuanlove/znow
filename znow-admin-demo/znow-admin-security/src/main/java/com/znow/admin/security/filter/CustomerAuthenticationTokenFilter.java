package com.znow.admin.security.filter;

import com.znow.admin.security.config.WebSecurityConfig;
import com.znow.admin.security.vo.UserVo;
import com.znow.common.utils.CookieUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomerAuthenticationTokenFilter extends OncePerRequestFilter {

    private static final Logger log = LoggerFactory.getLogger(CustomerAuthenticationTokenFilter.class);

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        String currentUrl = request.getRequestURL().toString();
        if (!this.authResourceUrls(request, WebSecurityConfig.noAuthRequiredResource())) {
            // 从cookie中获取token
            String userToken = CookieUtils.getCookieValue(request, "userToken", true);
            if (StringUtils.isNotBlank(userToken) && SecurityContextHolder.getContext().getAuthentication() == null) {
                UserVo userVo = (UserVo) redisTemplate.opsForValue().get("user:login:" + userToken);
                if (userVo != null) {
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userVo.getUsername(), userToken
                            , AuthorityUtils.createAuthorityList("role_admin"));
                    authentication.setDetails(userVo);
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                } else {
                    this.expiredTokenException(request, response);
                    //return;
                }
            }
        }
        chain.doFilter(request, response);
    }

    /**
     * token失效异常处理
     **/
    private void expiredTokenException(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.error("token失效");
        //response.sendRedirect(request.getContextPath() + "/login.html");
    }

    /**
     * 判断当前url是否为开放路径
     *
     * @param request
     * @param permitUrlArray 权限路径
     * @return 如果当前url符合规则返回true，否则返回false
     **/
    private boolean authResourceUrls(HttpServletRequest request, String[] permitUrlArray) {
        if (request == null) {
            return true;
        }
        // OPTIONS放过
        if (request.getMethod() != null && request.getMethod().toUpperCase().equals(HttpMethod.OPTIONS.name())) {
            return true;
        }
        if (null != permitUrlArray && permitUrlArray.length > 0) {
            for (String eachItem : permitUrlArray) {
                if (new AntPathRequestMatcher(eachItem).matches(request)) {
                    return true;
                }
            }
        }
        return false;
    }

}
