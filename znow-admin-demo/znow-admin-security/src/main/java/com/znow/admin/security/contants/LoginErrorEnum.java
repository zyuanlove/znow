package com.znow.admin.security.contants;

public enum LoginErrorEnum {

    CAPTCHA_CODE_ERROR(400000,"验证码错误！");

    LoginErrorEnum(int code,String desc){
        this.code = code;
        this.desc = desc;
    }

    private int code;

    private String desc;

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
