package com.znow.admin.security.service.impl;

import com.znow.admin.security.dao.UserMapper;
import com.znow.admin.security.entity.UserBean;
import com.znow.admin.security.service.IUserService;
import com.znow.admin.security.vo.UserVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserBean userBean = userMapper.findByName(username);
        if (userBean == null) {
            throw new UsernameNotFoundException("账号名或密码不存在！");
        }
        UserVo userVo = new UserVo();
        BeanUtils.copyProperties(userBean, userVo);
        userVo.setAuthorities(AuthorityUtils.createAuthorityList("admin"));
        userVo.setAccountNonLocked(true);
        userVo.setCredentialsNonExpired(true);
        userVo.setAccountNonExpired(true);
        userVo.setEnabled(true);
        return userVo;
    }

}
