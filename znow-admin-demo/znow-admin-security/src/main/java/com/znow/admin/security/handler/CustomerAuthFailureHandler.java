package com.znow.admin.security.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.znow.admin.security.exception.LoginParamException;
import com.znow.admin.security.exception.ValidCodeException;
import com.znow.common.vo.ServerResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * TODO
 *
 * @author yuan 2018/12/12 10:59
 */
@Component
public class CustomerAuthFailureHandler implements AuthenticationFailureHandler {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        response.setStatus(HttpStatus.OK.value());
        response.setContentType("application/json;charset=UTF-8");
        // 账号不可用
        this.errorResponse(response,e);
    }

    private void errorResponse(HttpServletResponse response,AuthenticationException e) throws IOException {
        if(e instanceof ValidCodeException){
            response.getWriter().write(objectMapper.writeValueAsString(ServerResponse.createByErrorCodeMessage(400003,e.getMessage())));
        }else if(e instanceof BadCredentialsException){
            response.getWriter().write(objectMapper.writeValueAsString(ServerResponse.createByErrorCodeMessage(400005,e.getMessage())));
        }else {
            response.getWriter().write(objectMapper.writeValueAsString(ServerResponse.createByErrorMessage(e.getMessage())));
        }
    }

}
