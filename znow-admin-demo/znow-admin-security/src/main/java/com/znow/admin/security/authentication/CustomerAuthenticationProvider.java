package com.znow.admin.security.authentication;

import com.znow.admin.security.exception.LoginParamException;
import com.znow.admin.security.exception.ValidCodeException;
import com.znow.admin.security.param.LoginParam;
import com.znow.admin.security.service.IUserService;
import com.znow.admin.security.vo.UserVo;
import com.znow.common.utils.MD5Util;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * TODO
 *
 * @author yuan 2018/12/10 16:38
 */
public class CustomerAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private IUserService userDetailsService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UsernamePasswordAuthenticationToken authenticationToken = (UsernamePasswordAuthenticationToken) authentication;
        LoginParam loginParam = (LoginParam) authentication.getCredentials();
        if (loginParam == null) {
            throw new LoginParamException("登录参数为空！");
        }
        if (StringUtils.isBlank(loginParam.getUsername())) {
            throw new LoginParamException("账户名不能为空！");
        }
        if (StringUtils.isBlank(loginParam.getPassword())) {
            throw new LoginParamException("密码不能为空！");
        }
        if (StringUtils.isBlank(loginParam.getCaptchaToken())) {
            throw new LoginParamException("验证码token不能为空");
        }
        if (StringUtils.isBlank(loginParam.getCaptchaCode())) {
            throw new LoginParamException("验证码不能为空");
        }
        String captchaCode = redisTemplate.opsForValue().get("user:captcha:" + loginParam.getCaptchaToken());
        if (StringUtils.isBlank(captchaCode)) {
            throw new ValidCodeException("验证码过期或不存在请重新获取！");
        }
        if (!StringUtils.equalsIgnoreCase(captchaCode, loginParam.getCaptchaCode())) {
            throw new ValidCodeException("验证码错误！");
        } else {
            // 验证成功删除验证码
            redisTemplate.delete("user:captcha:" + loginParam.getCaptchaToken());
        }
        UserVo userDetails = (UserVo) userDetailsService.loadUserByUsername(loginParam.getUsername());
        if (userDetails == null) {
            throw new BadCredentialsException("账号名或密码错误！");
        }
        if (!MD5Util.md5Encode(loginParam.getPassword()).equals(userDetails.getPassword())) {
            throw new BadCredentialsException("账号名或密码错误！");
        }
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails.getUsername(), loginParam, AuthorityUtils.createAuthorityList("role_admin"));
        userDetails.setPassword(StringUtils.EMPTY);
        usernamePasswordAuthenticationToken.setDetails(userDetails);
        return usernamePasswordAuthenticationToken;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }

    public UserDetailsService getUserDetailsService() {
        return userDetailsService;
    }

    public void setUserDetailsService(IUserService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

}
