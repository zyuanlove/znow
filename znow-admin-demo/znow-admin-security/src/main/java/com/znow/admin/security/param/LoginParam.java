package com.znow.admin.security.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class LoginParam {

    @ApiModelProperty(value = "登录类型(password：密码登录；sms：手机验证码登录)")
    private String loginType;

    private String username;

    private String password;

    private String phone;

    private String captchaToken;

    @ApiModelProperty(value = "图形验证码")
    private String captchaCode;

    @ApiModelProperty(value = "短信验证码")
    private String smsCode;
}
