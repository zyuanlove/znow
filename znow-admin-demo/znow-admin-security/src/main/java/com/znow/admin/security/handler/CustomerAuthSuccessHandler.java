package com.znow.admin.security.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.znow.admin.security.vo.UserVo;
import com.znow.common.utils.CookieUtils;
import com.znow.common.utils.UUIDUtils;
import com.znow.common.vo.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * TODO
 *
 * @author yuan 2018/12/12 10:56
 */
@Component
public class CustomerAuthSuccessHandler implements AuthenticationSuccessHandler {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setStatus(HttpStatus.OK.value());
        response.setContentType("application/json;charset=UTF-8");
        UserVo userVo = (UserVo) authentication.getDetails();
        String token = this.generateToken(userVo);
        redisTemplate.opsForValue().set("user:login:" + token, userVo, 1800, TimeUnit.SECONDS);
        CookieUtils.setCookie(request, response, "userToken", token, 1800);
        response.getWriter().write(objectMapper.writeValueAsString(ServerResponse.createBySuccess(token)));
    }

    private String generateToken(UserVo userVo) {
        StringBuilder token = new StringBuilder();
        if (userVo != null) {
            String uid = userVo.getUserId().toString();
            token.append(uid).append(UUIDUtils.nextID());
        }
        return token.toString();
    }

}
