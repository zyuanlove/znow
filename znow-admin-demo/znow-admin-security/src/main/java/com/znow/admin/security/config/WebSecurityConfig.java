package com.znow.admin.security.config;

import com.google.common.collect.Lists;
import com.znow.admin.security.authentication.CustomerAuthenticationProvider;
import com.znow.admin.security.filter.CustomerAuthenticationFilter;
import com.znow.admin.security.filter.CustomerAuthenticationTokenFilter;
import com.znow.admin.security.handler.CustomAuthenticationEntryPoint;
import com.znow.admin.security.handler.CustomerAuthFailureHandler;
import com.znow.admin.security.handler.CustomerAuthSuccessHandler;
import com.znow.admin.security.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.List;

/**
 * springSecurity配置类
 *
 * @author yuan 2019/5/30 15:00
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomerAuthFailureHandler customerAuthFailureHandler;

    @Autowired
    private CustomerAuthSuccessHandler customerAuthSuccessHandler;

    @Autowired
    private IUserService userService;


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(customerAuthProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 启用iframe
        http.headers().frameOptions().disable().and()
                // 不使用session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                //防止csrf攻击
                .csrf().disable()
                //禁用缓存
                .headers().cacheControl();

        http.authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll().and()
                // 不需要认证的资源
                .authorizeRequests().antMatchers(noAuthRequiredResource()).permitAll().and()
                .authorizeRequests().anyRequest().authenticated()
                .and();
        //.formLogin().loginPage("/login.html").permitAll();
        //http.exceptionHandling().accessDeniedHandler(new CustomAccessDeniedHandler());
        http.exceptionHandling().authenticationEntryPoint(new CustomAuthenticationEntryPoint());
        http.addFilterBefore(customerAuthFilter(), UsernamePasswordAuthenticationFilter.class);
        http.addFilterBefore(customerAuthTokenFilterBean(), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Bean
    public CustomerAuthenticationProvider customerAuthProvider() {
        CustomerAuthenticationProvider customerAuthenticationProvider = new CustomerAuthenticationProvider();
        customerAuthenticationProvider.setUserDetailsService(this.userService);
        return customerAuthenticationProvider;
    }

    public CustomerAuthenticationFilter customerAuthFilter() throws Exception {
        CustomerAuthenticationFilter customLoginAuthenticationFilter = new CustomerAuthenticationFilter(authenticationManager());
        customLoginAuthenticationFilter.setAuthenticationSuccessHandler(customerAuthSuccessHandler);
        customLoginAuthenticationFilter.setAuthenticationFailureHandler(customerAuthFailureHandler);
        return customLoginAuthenticationFilter;
    }

    @Bean
    public CustomerAuthenticationTokenFilter customerAuthTokenFilterBean() {
        return new CustomerAuthenticationTokenFilter();
    }

    public static String[] noAuthRequiredResource() {
        List<String> noAuthRequires = Lists.newArrayList(
                "/**/*.js",
                "/**/*.css",
                "/**/*.uijs",
                "/**/*.uicss",
                "/**/*.woff2*",
                "/**/*.woff*",
                "/**/*.ttf*",
                "/**/*.png",
                "/**/*.gif",
                "/**/*.jpg",
                "/**/*.mp3",
                "/**/*.css.map",
                "/**/*.js.map",
                "/uiengine.uijs",
                "/uiengine.uicss",
                "/**/*.ico",
                "/v2/api-docs",
                "/swagger/**",
                "/v2/api-docs",
                "/swagger-resources/**",
                "/swagger/**",
                "/actuator/**",
                "/common/**",
                "/captcha/**",
                "/login.html"
        );

        String[] antResources = new String[noAuthRequires.size()];
        return noAuthRequires.toArray(antResources);
    }

}
