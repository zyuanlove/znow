package com.znow.file.controller;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.PutObjectResult;
import com.znow.file.common.OSSClientUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@Slf4j
public class FileUploadController {

    /**
     * 阿里云API的bucket名称
     */

    @Value("${aliyun.oss.bucketName}")
    private String bucketName;
    /**
     * 阿里云API的文件夹名称
     */
    private String folder = "goods_cate/";

    @Autowired
    private OSSClientUtil ossClientUtil;

    @PostMapping("/upload")
    public ResponseEntity fileUpload(@RequestParam("file") MultipartFile file, @RequestParam("cateName") String cateName) throws IOException {
        String fileName = file.getOriginalFilename();
        //扩展名
        //abc.jpg
        String fileExtensionName = fileName.substring(fileName.lastIndexOf(".") + 1);

        String firstNamePre = fileName.replace("."+fileExtensionName,"");

        String uploadFileName = UUID.randomUUID().toString() + "." + fileExtensionName;

        //StringUtils.isNumeric()
        String cateNewName = splitNotNumber(firstNamePre).trim();
        //log.info("开始上传文件,上传文件的文件名:{},上传的路径:{},新文件名:{}", fileName, path, uploadFileName);
        /*File fileDir = new File(path);
        if (!fileDir.exists()) {
            fileDir.setWritable(true);
            fileDir.mkdirs();
        }*/
        String fileUrl = folder + uploadFileName;
        OSSClient ossClient = ossClientUtil.getOSSClient();
        PutObjectResult putObjectResult = ossClient.putObject(bucketName, fileUrl, file.getInputStream());
        if (StringUtils.isNotBlank(putObjectResult.getETag())) {
            Map<String, Object> map = new HashMap<>();
            map.put("code", 0);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("update ys_goods_cate set cate_mini_images = '"+this.newfileUrl(fileUrl)+"'").append(",")
            .append("cate_name = ").append("'"+cateNewName+"'").append(" ").append("where cate_id = '"+cateName+"'"+";");
            map.put("data", stringBuilder.toString());
            return ResponseEntity.ok(map);
        }
        return null;
    }

    private String newfileUrl(String fileUrl) {
        if (StringUtils.isNotBlank(fileUrl)) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("http://img.inglemirepharmmall.com")
                    .append("/")
                    .append(fileUrl).append("?x-oss-process=style/q60");
            return stringBuilder.toString();
        }
        return null;
    }

    // 截取非数字
    public static String splitNotNumber(String content) {
        Pattern pattern = Pattern.compile("\\D+");
        Matcher matcher = pattern.matcher(content);
        while (matcher.find()) {
            return matcher.group(0);
        }
        return "";
    }
}
