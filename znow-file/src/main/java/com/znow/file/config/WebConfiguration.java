package com.znow.file.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * web 环境配置项
 *
 * @author liujia 2018/3/30 15:25
 */
@Configuration
public class WebConfiguration implements WebMvcConfigurer {

    /**
     * 测试环境可以是*，正式环境必须配置对应的环境和域名
     */
    @Value("${yingke.security.allow-origins:*}")
    private String allowedOrigins;

    @Bean
    public CorsFilter corsFilter() {
        String[] origins = StringUtils.split(allowedOrigins, ",");
        //设置允许跨域的请求来源，防止跨域攻击
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        for (String origin : origins) {
            corsConfiguration.addAllowedOrigin(origin);
        }
        corsConfiguration.addAllowedHeader("*");
        corsConfiguration.addAllowedMethod("*");

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", corsConfiguration); // 4
        return new CorsFilter(source);
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedOrigins("*")
                .allowedMethods("GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS")
                .allowCredentials(false).maxAge(3600);
    }

}
