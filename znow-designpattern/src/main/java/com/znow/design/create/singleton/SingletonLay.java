package com.znow.design.create.singleton;


/**
 * @throws
 */
public class SingletonLay {

    private SingletonLay() {
    }

    private volatile static SingletonLay instance = null;

    public synchronized static SingletonLay getInstance() {
        if (instance == null) {
            instance = new SingletonLay();
        }
        return instance;
    }

    // 双重检查

    public static SingletonLay checkDoubleGetInstance() {
        if (instance == null) {
            synchronized (SingletonLay.class) {
                if (instance == null) {
                    instance = new SingletonLay();
                    // 分配内存地址给对象
                    // 初始化对象
                    // 设置指向映射对象
                }
            }
        }
        return instance;
    }
}
