package com.znow.design.create.property;

import java.io.*;

public class Book implements Cloneable, Serializable {

    private Integer id;

    private String bookName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public Book(Integer id, String bookName) {
        this.id = id;
        this.bookName = bookName;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", bookName='" + bookName + '\'' +
                '}';
    }

    /**
     * 浅拷贝
     */
    @Override
    public Book clone() {
        Book book = null;
        if (book == null) {
            try {
                book = (Book) super.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }
        return book;
    }

    public Book deepClone() throws IOException, ClassNotFoundException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(bos);
        objectOutputStream.writeObject(this);

        ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bis);
        return (Book) ois.readObject();
    }
}
