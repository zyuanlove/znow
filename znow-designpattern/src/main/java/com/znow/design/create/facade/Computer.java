package com.znow.design.create.facade;

public class Computer {

    private Cpu cpu;

    private Memory memory;

    public Computer(){
        cpu = new Cpu();
        memory = new Memory();
    }

    public void startUp(){
        System.out.println("电脑开启......");
        cpu.startUp();
        cpu.finish();
        memory.startUp();
        memory.finish();
    }

    public static void main(String[] args) {
        Computer computer = new Computer();
        computer.startUp();
    }
}
