package com.znow.design.create.factory.methodFactory.demo;

public  abstract class SendMessage {

    public abstract void sendMessage();
}
