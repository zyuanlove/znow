package com.znow.design.create.factory.abstractFactory;

public class Test {

    public static void main(String[] args) {
        JavaCourseFactory javaCourseFactory = new JavaCourseFactory();
        Video video = javaCourseFactory.getVideo();
        video.process();
    }
}
