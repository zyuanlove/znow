package com.znow.design.create.singleton;

import java.net.URLStreamHandlerFactory;

/**
 * 饿汉式单例模式
 */
public class Singleton {

    private Singleton(){}

    private static final Singleton instance = new Singleton();

    public static Singleton getInstance() {
        return instance;
    }

    public static void main(String[] args) {
        Singleton.getInstance();
    }
}
