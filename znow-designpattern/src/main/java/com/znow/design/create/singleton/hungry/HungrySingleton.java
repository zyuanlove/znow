package com.znow.design.create.singleton.hungry;

import java.io.Serializable;

/**
 * 饿汉单例模式
 */
public class HungrySingleton implements Serializable {

    private HungrySingleton() {
    }

    private static final HungrySingleton instance = new HungrySingleton();

    public static HungrySingleton getInstance() {
        return instance;
    }
}
