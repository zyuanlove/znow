package com.znow.design.create.facade;

public class Cpu {

    public void startUp(){
        System.out.println("cpu 开启......");
    }

    public void finish(){
        System.out.println("cpu 启动完成......");
    }
}
