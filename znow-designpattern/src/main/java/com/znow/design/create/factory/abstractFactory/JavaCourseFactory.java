package com.znow.design.create.factory.abstractFactory;

public class JavaCourseFactory extends CourseFactory {

    @Override
    public Video getVideo() {
        return new JavaVideo();
    }

    @Override
    public Article getArticle() {
        return null;
    }
}
