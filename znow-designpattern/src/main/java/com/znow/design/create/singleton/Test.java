package com.znow.design.create.singleton;

public class Test {

    public static void main(String[] args) {
        Thread t1 = new Thread(new Runnable() {
            public void run() {
                SingletonLay singletonLay = SingletonLay.getInstance();
                System.out.println(Thread.currentThread().getName() + singletonLay);
            }
        });

        Thread t2 = new Thread(new Runnable() {
            public void run() {
                SingletonLay singletonLay = SingletonLay.getInstance();
                System.out.println(Thread.currentThread().getName() + singletonLay);
            }
        });

        t1.start();
        t2.start();

        System.out.println("end");
    }
}
