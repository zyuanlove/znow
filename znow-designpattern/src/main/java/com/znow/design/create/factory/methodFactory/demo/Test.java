package com.znow.design.create.factory.methodFactory.demo;

public class Test {

    public static void main(String[] args) {
        MailMessageFactory messageFactory = new MailMessageFactory();
        SendMessage message = messageFactory.getMessage();
        message.sendMessage();
    }
}
