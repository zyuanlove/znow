package com.znow.design.create.property;

import java.io.IOException;

public class PropertyTest {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Book book = new Book(1,"三体");
        // 浅拷贝
        Book clone1 = book.clone();
        // 浅拷贝
        Book clone2 = book.clone();

        System.out.println("比较克隆后的对象与源对象是否相等："+ clone1.equals(book));
        System.out.println("源对象hashcode：" + book.hashCode() + "对象："+ book.toString());
        System.out.println("克隆后的对象1hashcode：" + clone1.hashCode() + "对象："+ clone1.toString());
        System.out.println("克隆后的对象2hashcode：" + clone2.hashCode() + "对象："+ clone2.toString());

        Book book1 = book.deepClone();
        System.out.println("深克隆与源对象比较：" + book1.equals(book));
        System.out.println("深克隆："+  book1.toString());

    }
}
