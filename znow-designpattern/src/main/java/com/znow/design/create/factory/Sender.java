package com.znow.design.create.factory;


/**
 * 工厂模式
 *
 * @author yuan 2019/6/12 14:55
 */
public interface Sender {

    void sendMessage();

}
