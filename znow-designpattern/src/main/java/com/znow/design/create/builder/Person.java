package com.znow.design.create.builder;

public class Person {

    private String name;

    private Integer age;

    private Integer sex;

    private Integer id;

    private Double height;

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sex=" + sex +
                ", id=" + id +
                ", height=" + height +
                '}';
    }

    public Person(Builder builder) {
        this.name = builder.name;
        this.age = builder.age;
        this.sex = builder.sex;
        this.id = builder.id;
        this.height = builder.height;
    }

    public static class Builder {

        private String name;

        private Integer age;

        private Integer sex;

        private Integer id;

        private Double height;

        public Builder(String name){
            this.name = name;
        }

        public Builder age(Integer age) {
            this.age = age;
            return this;
        }

        public Builder sex(Integer sex) {
            this.sex = sex;
            return this;
        }

        public Person build(){
           return  new Person(this);
        }
    }

    public static void main(String[] args) {
        Person person = new Person.Builder("zt").sex(1).age(18).build();
        System.out.println(person);
    }
}
