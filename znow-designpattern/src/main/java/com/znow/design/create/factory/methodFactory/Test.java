package com.znow.design.create.factory.methodFactory;

public class Test {

    public static void main(String[] args) {
        VedioFactory vedioFactory = new JavaVedioFactoy();

        JavaVedio vedio = vedioFactory.getVedio();
        vedio.process();
    }
}
