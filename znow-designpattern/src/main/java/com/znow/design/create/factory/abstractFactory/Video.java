package com.znow.design.create.factory.abstractFactory;

public abstract class Video {

  public abstract void process();
}
