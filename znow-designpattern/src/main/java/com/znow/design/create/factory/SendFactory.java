package com.znow.design.create.factory;

import java.util.Calendar;

/**
 *  工厂模式
 */
public class SendFactory {

    public Sender createMessage(String type){
        if("mail".equals(type)){
            return new MailSender();
        }else if("sms".equals(type)){
            return new SmsSender();
        }else {
            return null;
        }
    }

    public static void main(String[] args) {
        SendFactory factory = new SendFactory();
        Sender sms = factory.createMessage("sms");
        sms.sendMessage();
    }
}
