package com.znow.design.create.factory.abstractFactory;

public abstract class CourseFactory {

    public abstract Video getVideo();

    public abstract Article getArticle();
}
