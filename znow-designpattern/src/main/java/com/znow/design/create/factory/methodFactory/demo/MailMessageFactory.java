package com.znow.design.create.factory.methodFactory.demo;

public class MailMessageFactory extends  MessageFactory {

    @Override
    public SendMessage getMessage() {
        return new MailMessage();
    }
}
