package com.znow.design.create.factory.methodFactory.demo;

public abstract class MessageFactory {

    public abstract SendMessage getMessage();
}
