package com.znow.design.principle.dependency_inversion;

/**
 * 抽象学习课程
 *
 * @author yuan 2019/8/9 10:57
 */
public interface ICourse {

    void studyCourse();
}
