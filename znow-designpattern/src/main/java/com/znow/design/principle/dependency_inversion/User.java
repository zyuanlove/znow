package com.znow.design.principle.dependency_inversion;

/**
 * 依赖倒置原则例子
 *
 * 高层模块不应该依赖底层模式，二者应该通过抽象依赖
 *
 * @author yuan 2019/8/9 10:52
 */
public class User {

    /**
     * @since v1
     */
    public void studyJavaCourse(){
        System.out.println("用户学习了Java课程......");
    }


    /**
     * @since v1
     */
    public void studyPyCourse(){
        System.out.println("用户学习了Python课程......");
    }

    /**
     * 依赖与抽象层
     *
     * @since 2.0
     */
    public void studyCourse(ICourse course){
        course.studyCourse();
    }

}
