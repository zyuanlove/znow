package com.znow.design.principle.dependency_inversion;

import com.znow.design.principle.dependency_inversion.User;

public class Test {

    public static void main(String[] args) {
       /* User user = new User();
        user.studyJavaCourse();
        user.studyPyCourse();*/

        // test高层模块去选择课程
        User user = new User();
        // 抽象层模块
        JavaCourse javaCourse = new JavaCourse();
        PyCourse pyCourse = new PyCourse();

        user.studyCourse(javaCourse);
        user.studyCourse(pyCourse);
    }
}

