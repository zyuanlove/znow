package com.znow.design.cl;

import java.util.HashMap;
import java.util.Map;

public class EventStrategyService {

    private Map<String, IBaseService> eventServiceMap = new HashMap<>();

    public EventStrategyService(IBaseService item) {
        eventServiceMap.put(item.getEventType(), item);
    }

    public void doEvent(String eventType) {
        IBaseService baseService = eventServiceMap.get(eventType);
        if (baseService == null) {

        }
        baseService.doEvent(eventType);
    }
}
