package com.znow.design.cl;

/**
 * 策略模式
 *
 * @author yuan 2019/11/28 15:27
 */
public interface IBaseService {

    /**
     * 事件类型
     */
    String getEventType();

    void doEvent(String eventType);
}
