package com.znow.design.cl;

public class UserAddServiceImpl implements IBaseService {

    @Override
    public String getEventType() {
        return "add";
    }

    @Override
    public void doEvent(String eventType) {
        if(eventType.equals(getEventType())){
            System.out.println("添加用户");
        }
    }
}
