package com.znow.payment.web.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.AlipayResponse;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradePayModel;
import com.alipay.api.domain.AlipayTradePrecreateModel;
import com.alipay.api.domain.ExtendParams;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePayRequest;
import com.alipay.api.response.AlipayTradePayResponse;
import com.alipay.api.response.AlipayTradePrecreateResponse;
import com.alipay.demo.trade.config.Configs;
import com.alipay.demo.trade.model.GoodsDetail;
import com.alipay.demo.trade.model.builder.AlipayTradePrecreateRequestBuilder;
import com.alipay.demo.trade.model.result.AlipayF2FPrecreateResult;
import com.alipay.demo.trade.service.AlipayTradeService;
import com.alipay.demo.trade.service.impl.AlipayTradeServiceImpl;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.znow.payment.server.config.AliPayConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 支付接口
 *
 * @author yuan 2019/5/17 13:30
 */
@RestController
@RequestMapping("/payment")
@Slf4j
public class PaymentController {

    private static AlipayTradeService tradeService;

    static {
        Configs.init("zfbinfo.properties");
        /** 使用Configs提供的默认参数
         *  AlipayTradeService可以使用单例或者为静态成员对象，不需要反复new
         */
        tradeService = new AlipayTradeServiceImpl.ClientBuilder().build();
    }

    @RequestMapping("/create/alipay")
    public void createPay() {
        // (必填) 商户网站订单系统中唯一订单号，64个字符以内，只能包含字母、数字、下划线，
        // 需保证商户系统端不能重复，建议通过数据库sequence生成，
        String outTradeNo = "tradepay" + System.currentTimeMillis()
                + (long) (Math.random() * 10000000L);

        // (必填) 订单标题，粗略描述用户的支付目的。如“xxx品牌xxx门店消费”
        String subject = "当面付测试接口test";

        // (必填) 订单总金额，单位为元，不能超过1亿元
        // 如果同时传入了【打折金额】,【不可打折金额】,【订单总金额】三者,则必须满足如下条件:【订单总金额】=【打折金额】+【不可打折金额】
        String totalAmount = "1000";

        // (必填) 付款条码，用户支付宝钱包手机app点击“付款”产生的付款条码
        String authCode = "用户自己的支付宝付款码"; // 条码示例，286648048691290423
        // (可选，根据需要决定是否使用) 订单可打折金额，可以配合商家平台配置折扣活动，如果订单部分商品参与打折，可以将部分商品总价填写至此字段，默认全部商品可打折
        // 如果该值未传入,但传入了【订单总金额】,【不可打折金额】 则该值默认为【订单总金额】- 【不可打折金额】
        //        String discountableAmount = "1.00"; //

        // (可选) 订单不可打折金额，可以配合商家平台配置折扣活动，如果酒水不参与打折，则将对应金额填写至此字段
        // 如果该值未传入,但传入了【订单总金额】,【打折金额】,则该值默认为【订单总金额】-【打折金额】
        String undiscountableAmount = "0.0";

        // 卖家支付宝账号ID，用于支持一个签约账号下支持打款到不同的收款账号，(打款到sellerId对应的支付宝账号)
        // 如果该字段为空，则默认为与支付宝签约的商户的PID，也就是appid对应的PID
        String sellerId = "";

        // 订单描述，可以对交易或商品进行一个详细地描述，比如填写"购买商品3件共20.00元"
        String body = "购买商品小米手机1000元";

        // 商户操作员编号，添加此参数可以为商户操作员做销售统计
        String operatorId = "test_operator_id";

        // (必填) 商户门店编号，通过门店号和商家后台可以配置精准到门店的折扣信息，详询支付宝技术支持
        String storeId = "test_store_id";

        // 业务扩展参数，目前可添加由支付宝分配的系统商编号(通过setSysServiceProviderId方法)，详情请咨询支付宝技术支持
        String providerId = "2088102177874342";
       /* ExtendParams extendParams = new ExtendParams();
        extendParams.setSysServiceProviderId(providerId);*/

        // 支付超时，线下扫码交易定义为5分钟
        String timeoutExpress = "5m";

       /* // 商品明细列表，需填写购买商品详细信息，
        List<GoodsDetail> goodsDetailList = new ArrayList<GoodsDetail>();
        // 创建一个商品信息，参数含义分别为商品id（使用国标）、名称、单价（单位为分）、数量，如果需要添加商品类别，详见GoodsDetail
        GoodsDetail goods1 = GoodsDetail.newInstance("goods_id001123", "小米手机123", 1000, 1);
        // 创建好一个商品后添加至商品明细列表
        goodsDetailList.add(goods1);*/
        String appAuthToken = "应用授权令牌";//根据真实值填写

        AlipayClient alipayClient = DefaultAlipayClient.builder(AliPayConfig.gatewayUrl,
                AliPayConfig.appid, AliPayConfig.privateKey)
                .alipayPublicKey(AliPayConfig.alipayPublicKey)
                .format(AliPayConfig.format)
                .charset(AliPayConfig.charset)
                .signType(AliPayConfig.signType)
                .build();

        AlipayTradePrecreateModel tradePayModel = new AlipayTradePrecreateModel();

        tradePayModel.setSubject(subject);
        tradePayModel.setTotalAmount(totalAmount);
        tradePayModel.setOutTradeNo(outTradeNo);
        tradePayModel.setUndiscountableAmount(undiscountableAmount);
        tradePayModel.setSellerId(sellerId);
        tradePayModel.setBody(body);
        tradePayModel.setOperatorId(operatorId);
        tradePayModel.setStoreId(storeId);
        //tradePayModel.setExtendParams(extendParams);
        tradePayModel.setTimeoutExpress(timeoutExpress);


        List<com.alipay.api.domain.GoodsDetail> goodsDetails = Lists.newArrayList();
        com.alipay.api.domain.GoodsDetail goodsDetail = new com.alipay.api.domain.GoodsDetail();
        goodsDetail.setGoodsId("goods_id001123");
        goodsDetail.setGoodsName("给你赚钱了");
        goodsDetail.setPrice("1000");
        goodsDetail.setQuantity(1L);

        goodsDetails.add(goodsDetail);

        tradePayModel.setGoodsDetail(goodsDetails);

        AlipayTradePayRequest request = new AlipayTradePayRequest();
        request.setBizModel(tradePayModel);
        request.setNotifyUrl("http://yuan.vipgz1.idcfengye.com/payment/alipay/callback");

        try {
            AlipayResponse response = alipayClient.execute(request);

            System.out.println(new Gson().toJson(response));

            System.out.println(response.getBody().toString());
            System.out.println(response.isSuccess());

        } catch (AlipayApiException e) {
            System.out.println(e.getErrMsg());
            e.printStackTrace();
        }

        // 创建扫码支付请求builder，设置请求参数
       /* AlipayTradePrecreateRequestBuilder builder = new AlipayTradePrecreateRequestBuilder()
                .setSubject(subject).setTotalAmount(totalAmount).setOutTradeNo(outTradeNo)
                .setUndiscountableAmount(undiscountableAmount).setSellerId(sellerId).setBody(body)
                .setOperatorId(operatorId).setStoreId(storeId)
                .setTimeoutExpress(timeoutExpress)
                .setNotifyUrl("http://yuan.vipgz1.idcfengye.com/payment/alipay/callback")//支付宝服务器主动通知商户服务器里指定的页面http路径,根据需要设置
                .setGoodsDetailList(goodsDetailList);

        // 调用tradePay方法获取当面付应答
        AlipayF2FPrecreateResult result = tradeService.tradePrecreate(builder);
        switch (result.getTradeStatus()) {
            case SUCCESS:
                log.info("支付宝支付成功: )");

                System.out.println("支付成功");
                break;

            case FAILED:
                log.error("支付宝支付失败!!!");
                break;

            case UNKNOWN:
                log.error("系统异常，订单状态未知!!!");
                break;

            default:
                log.error("不支持的交易状态，交易返回异常!!!");
                break;
        }*/
    }

    @RequestMapping("/alipay")
    public void payMent(){
        AlipayClient alipayClient = new DefaultAlipayClient(AliPayConfig.gatewayUrl,AliPayConfig.appid,AliPayConfig.privateKey,
                AliPayConfig.format,AliPayConfig.charset,AliPayConfig.alipayPublicKey,AliPayConfig.signType);

        AlipayTradePayRequest request = new AlipayTradePayRequest();
        //request.setBizContent();

    }

    @RequestMapping("/alipay/callback")
    public String aliCallback(HttpServletRequest request) {
        Map<String, String> params = Maps.newHashMap();

        Map requestParams = request.getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            params.put(name, valueStr);
        }
        log.info("支付宝回调,sign:{},trade_status:{},参数:{}", params.get("sign"), params.get("trade_status"), params.toString());

        //非常重要,验证回调的正确性,是不是支付宝发的.并且呢还要避免重复通知.

        params.remove("sign_type");
        try {
            boolean alipayRSACheckedV2 = AlipaySignature.rsaCheckV2(params, Configs.getAlipayPublicKey(), "utf-8", Configs.getSignType());
            if (!alipayRSACheckedV2) {
                System.out.println("非法请求,验证不通过,再恶意请求我就报警找网警了");
            }
        } catch (AlipayApiException e) {
            log.error("支付宝验证回调异常", e);
        }

        //todo 验证各种数据

        //
        return "success";
    }

    public void pay() {

    }

}
