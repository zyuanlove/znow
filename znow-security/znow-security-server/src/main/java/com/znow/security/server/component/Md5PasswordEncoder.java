package com.znow.security.server.component;

import com.znow.common.utils.MD5Util;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * TODO
 *
 * @author dell 2018/11/8 9:57
 */
public class Md5PasswordEncoder implements PasswordEncoder {

    @Override
    public String encode(CharSequence charSequence) {
        return charSequence.toString();
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return encodedPassword.equals(MD5Util.md5Encode(rawPassword.toString()));
    }
}
