package com.znow.security.server.service.impl;

import com.znow.security.server.entity.UserBean;
import com.znow.security.server.mapper.UserMapper;
import com.znow.security.server.vo.UserVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserDetailsService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) {
        UserBean userBean = userMapper.findByName(username);
        if (userBean == null) {
            throw new UsernameNotFoundException("账号名或密码不存在！");
        }
        UserVo userVo = new UserVo();
        BeanUtils.copyProperties(userBean, userVo);
        userVo.setAuthorities(AuthorityUtils.createAuthorityList("admin"));
        return userVo;
    }

}
