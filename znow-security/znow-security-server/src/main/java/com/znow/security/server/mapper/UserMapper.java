package com.znow.security.server.mapper;

import com.znow.security.server.entity.UserBean;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper {

    UserBean findByName(String username);

    /**
     * 根据ID删除
     *
     * @param userId 主键ID
     */
    int deleteByPrimaryKey(Integer userId);

    /**
     * 添加对象所有字段
     *
     * @param record 插入字段对象(必须含ID）
     */
    int insert(UserBean record);

    /**
     * 添加对象所有字段
     *
     * @param record 插入字段对象(必须含ID）
     */
    int insertSelective(UserBean record);

    /**
     * 根据ID查询
     *
     * @param userId 主键ID
     */
    UserBean selectByPrimaryKey(Integer userId);

    /**
     * 根据ID修改对应字段
     *
     * @param record 修改字段对象(必须含ID）
     */
    int updateByPrimaryKeySelective(UserBean record);

    /**
     * 根据ID修改所有字段(必须含ID）
     *
     * @param record 修改字段对象(必须含ID）
     */
    int updateByPrimaryKey(UserBean record);

    /**
     * 分页查询
     */
    java.util.List<UserBean> selectByPageNumSize();
}