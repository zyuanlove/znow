package com.znow.security.server.constans.enums;

import com.znow.common.constans.BaseEnum;

/**
 * 登录类型枚举类
 *
 * @author yuan 2019/5/24 10:00
 */
public enum LoginType implements BaseEnum {

    /**
     * 密码登录
     */
    LOGIN_PASSWORD("password", "密码登录"),

    /**
     * 手机验证码登录
     */
    LOGIN_SMS_CODE("sms", "手机验证码登录");

    LoginType(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    private String name;

    private String desc;

    @Override
    public int getCode() {
        return 0;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }

}
