package com.znow.security.web.filter.mobile;

import com.znow.security.server.param.LoginParam;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义登录过滤器
 *
 * @author yuan 2018/12/7 16:54
 */
public class SmsAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private boolean postOnly = true;

    private AuthenticationManager authenticationManager;

    public SmsAuthenticationFilter(AuthenticationManager authenticationManager) {
        super(new AntPathRequestMatcher("/login/phone", "POST"));
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        //判断如果是POST提交
        if (this.postOnly && !request.getMethod().equals(HttpMethod.POST.name())) {
            throw new AuthenticationServiceException("Authentication methodFactory not supported: " + request.getMethod());
        }
        String phone = request.getParameter("phone");
        String smsCode = request.getParameter("smsCode");

        LoginParam loginParam = getLoginParam(phone, smsCode);

        SmsAuthenticationToken authRequest = new SmsAuthenticationToken(loginParam);
        this.setDetails(request, authRequest);
        return this.authenticationManager.authenticate(authRequest);
    }

    protected void setDetails(HttpServletRequest request,
                              SmsAuthenticationToken authRequest) {
        authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
    }

    @Override
    public void setAuthenticationSuccessHandler(AuthenticationSuccessHandler successHandler) {
        super.setAuthenticationSuccessHandler(successHandler);
    }

    @Override
    public void setFilterProcessesUrl(String filterProcessesUrl) {
        super.setFilterProcessesUrl(filterProcessesUrl);
    }

    private LoginParam getLoginParam(String phone, String smsCode) {
        LoginParam loginParam = new LoginParam();
        loginParam.setPhone(phone);
        loginParam.setSmsCode(smsCode);
        return loginParam;
    }
}
