package com.znow.security.web.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.znow.common.vo.ServerResponse;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * TODO
 *
 * @author yuan 2018/12/12 10:59
 */
@Component
public class CustomerAuthenticationFailureHandler implements AuthenticationFailureHandler {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        // response.setStatus(HttpStatus.FORBIDDEN.value());
        response.setContentType("application/json;charset=UTF-8");
        // 账号不可用
        if(e instanceof DisabledException){
            response.getWriter().write(objectMapper.writeValueAsString(ServerResponse.createByErrorCodeMessage(400001,e.getMessage())));
            return;
        }else{
            response.getWriter().write(objectMapper.writeValueAsString(ServerResponse.createByErrorMessage(e.getMessage())));
        }
    }

}
