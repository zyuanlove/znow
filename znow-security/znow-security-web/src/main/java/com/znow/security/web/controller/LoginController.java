package com.znow.security.web.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TODO
 *
 * @author yuan 2018/12/5 14:34
 */
@Slf4j
@RequestMapping("/")
@RestController
public class LoginController {

    @RequestMapping("login")
    public String login() {
        return "hello login";
    }
}
