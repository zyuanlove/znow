package com.znow.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.core.context.SecurityContextHolder;

@SpringBootApplication
public class SecurityApplication {

    public static void main(String[] args) {
        SecurityContextHolder.getContext().getAuthentication();
        SpringApplication.run(SecurityApplication.class, args);
    }

}
