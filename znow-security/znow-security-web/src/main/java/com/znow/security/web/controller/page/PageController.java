package com.znow.security.web.controller.page;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * TODO
 *
 * @author yuan 2018/12/7 15:43
 */
@Controller
public class PageController {

    @RequestMapping("/")
    public String index(){
        return "index";
    }

    @RequestMapping("/login.html")
    public String loginPage(){
        return "pages/login/login";
    }
}
