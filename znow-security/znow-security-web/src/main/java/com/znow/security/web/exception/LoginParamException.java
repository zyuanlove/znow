package com.znow.security.web.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * TODO
 *
 * @author yuan 2018/12/12 15:32
 */
public class LoginParamException extends AuthenticationException {

    private static final long serialVersionUID = -3117234398251492520L;

    private String code;

    private String msg;

    public LoginParamException(String code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }

    public LoginParamException(String msg) {
        super(msg);

    }

    public LoginParamException(String msg, Throwable throwable) {
        super(msg, throwable);
    }
}
