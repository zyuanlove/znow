package com.znow.security.web.filter;

import com.znow.security.server.param.LoginParam;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * TODO
 *
 * @author yuan 2018/12/7 18:02
 */
public class CustomerAuthenticationToken extends AbstractAuthenticationToken {

    private static final long serialVersionUID = 3328280673967966317L;

    private LoginParam loginParam;

    public CustomerAuthenticationToken(Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
    }

    public CustomerAuthenticationToken(LoginParam loginParam) {
        super(null);
        this.loginParam = loginParam;
        super.setAuthenticated(false);
    }

    public CustomerAuthenticationToken(LoginParam loginParam, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.loginParam = loginParam;
        super.setAuthenticated(true);
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        if (isAuthenticated) {
            throw new IllegalArgumentException(
                    "Cannot set this token to trusted - use constructor which takes a GrantedAuthority list instead");
        }
        super.setAuthenticated(false);
    }

    @Override
    public Object getCredentials() {
        return loginParam;
    }

    @Override
    public Object getPrincipal() {
        return loginParam.getUsername();
    }
}
