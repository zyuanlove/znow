package com.znow.security.web.filter.mobile;

import com.znow.security.server.param.LoginParam;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * 自定义手机号登录
 *
 * @author yuan 2018/12/12 16:01
 */
public class SmsAuthenticationToken extends AbstractAuthenticationToken {

    private static final long serialVersionUID = 9109325265708128760L;

    private LoginParam loginParam;

    public SmsAuthenticationToken(Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
    }

    public SmsAuthenticationToken(LoginParam loginParam) {
        super(null);
        this.loginParam = loginParam;
        super.setAuthenticated(false);
    }

    public SmsAuthenticationToken(LoginParam loginParam, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.loginParam = loginParam;
        super.setAuthenticated(true);
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        if (isAuthenticated) {
            throw new IllegalArgumentException(
                    "Cannot set this token to trusted - use constructor which takes a GrantedAuthority list instead");
        }
        super.setAuthenticated(false);
    }

    @Override
    public Object getCredentials() {
        return loginParam;
    }

    @Override
    public Object getPrincipal() {
        return loginParam.getUsername();
    }
}
