package com.znow.security.web.authentication;

import com.znow.security.server.component.Md5PasswordEncoder;
import com.znow.security.server.param.LoginParam;
import com.znow.security.server.vo.UserVo;
import com.znow.security.web.exception.LoginParamException;
import com.znow.security.web.filter.CustomerAuthenticationToken;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

/**
 * TODO
 *
 * @author yuan 2018/12/10 16:38
 */
@Component
public class CustomerAuthenticationProvider implements AuthenticationProvider {

    private UserDetailsService userDetailsService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        CustomerAuthenticationToken authenticationToken = (CustomerAuthenticationToken) authentication;
        LoginParam loginParam = (LoginParam) authentication.getCredentials();

        if (loginParam == null) {
            throw new LoginParamException("登录参数为空！");
        }
        if (StringUtils.isBlank(loginParam.getUsername())) {
            throw new LoginParamException("账户名为空！");
        }
        if (StringUtils.isBlank(loginParam.getPassword())) {
            throw new LoginParamException("密码为空！");
        }
        UserVo userDetails = (UserVo) userDetailsService.loadUserByUsername((String) authenticationToken.getPrincipal());

        if (userDetails == null) {
            throw new BadCredentialsException("账号名或密码错误！");
        }
        // 校验密码
        if (!new Md5PasswordEncoder().matches(loginParam.getPassword(), userDetails.getPassword())) {
            throw new BadCredentialsException("账号名或密码错误！");
        }
        CustomerAuthenticationToken customerAuthenticationToken = new CustomerAuthenticationToken(loginParam,
                userDetails.getAuthorities());
        userDetails.setPassword(StringUtils.EMPTY);
        loginParam.setPassword(StringUtils.EMPTY);
        customerAuthenticationToken.setDetails(userDetails);
        return customerAuthenticationToken;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return CustomerAuthenticationToken.class.isAssignableFrom(authentication);
    }

    public UserDetailsService getUserDetailsService() {
        return userDetailsService;
    }

    public void setUserDetailsService(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }
}
