package com.znow.security.web.config;

import com.znow.security.server.service.impl.UserServiceImpl;
import com.znow.security.web.filter.CustomerAuthenticationFilter;
import com.znow.security.web.authentication.CustomerAuthenticationProvider;
import com.znow.security.web.handler.CustomerAuthenticationFailureHandler;
import com.znow.security.web.handler.CustomerAuthenticationSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


/**
 * security安全配置
 *
 * @author yuan 2018/12/7 10:16
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserServiceImpl userDetailsService;

    @Autowired
    private SmsAuthenticationConfig smsAuthenticationConfig;

    @Autowired
    private CustomerAuthenticationFailureHandler customerAuthenticationFailureHandler;

    @Autowired
    private CustomerAuthenticationSuccessHandler customerAuthenticationSuccessHandler;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        CustomerAuthenticationProvider customerAuthenticationProvider = new CustomerAuthenticationProvider();
        customerAuthenticationProvider.setUserDetailsService(this.userDetailsService);
        auth.authenticationProvider(customerAuthenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                //禁用session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                //所有请求都需要认证
                .anyRequest().authenticated().and()
                .formLogin().disable()
                //防止csrf攻击
                .csrf().disable()
                //禁用缓存
                .headers().cacheControl();

        CustomerAuthenticationFilter customerAuthenticationFilter = new CustomerAuthenticationFilter(authenticationManager());
        customerAuthenticationFilter.setAuthenticationSuccessHandler(customerAuthenticationSuccessHandler);
        customerAuthenticationFilter.setAuthenticationFailureHandler(customerAuthenticationFailureHandler);
        http.addFilterBefore(customerAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
        http.apply(smsAuthenticationConfig);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        /**
         * 不过滤前端资源
         **/
        web.ignoring().antMatchers(HttpMethod.GET,
                "/**/*.js",
                "/**/*.css",
                "/**/*.html",
                "/**/*.uijs",
                "/**/*.uicss",
                "/**/*.woff2*",
                "/**/*.woff*",
                "/**/*.ttf*",
                "/**/*.png",
                "/**/*.gif",
                "/**/*.jpg",
                "/**/*.mp3",
                "/**/*.css.map",
                "/**/*.js.map",
                "/uiengine.uijs",
                "/uiengine.uicss",
                "/favicon.ico"
        );
    }

    /*使用redis作为session方式@Bean
    public SpringSessionBackedSessionRegistry sessionRegistry() {
        RedisOperationsSessionRepository redisOperationsSessionRepository = new RedisOperationsSessionRepository(redisTemplate);
        // session过期时间（单位/s）
        return new SpringSessionBackedSessionRegistry(redisOperationsSessionRepository);
    }*/
}
