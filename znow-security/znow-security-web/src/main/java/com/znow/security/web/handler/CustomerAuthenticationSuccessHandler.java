package com.znow.security.web.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.znow.common.utils.MD5Util;
import com.znow.common.utils.RandomUtils;
import com.znow.common.utils.UUIDUtils;
import com.znow.common.utils.snowflake.Sequence;
import com.znow.common.utils.snowflake.SequenceUtils;
import com.znow.common.vo.ServerResponse;
import com.znow.security.server.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * TODO
 *
 * @author yuan 2018/12/12 10:56
 */
@Component
public class CustomerAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setContentType("application/json;charset=UTF-8");
        UserVo details = (UserVo) authentication.getDetails();
        String token = UUIDUtils.getUUID();
        redisTemplate.opsForHash().put("login:" + token, "user", details);
        redisTemplate.expire("login:" + token, 30, TimeUnit.MINUTES);
        response.getWriter().write(objectMapper.writeValueAsString(ServerResponse.createBySuccess("登录成功")));
    }

}
