package com.znow.auth.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class UserController {

    @RequestMapping("/user/info")
    public ResponseEntity getUser(){
        return ResponseEntity.ok(SecurityContextHolder.getContext().getAuthentication());
    }
}
