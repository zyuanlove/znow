package com.znow.resource.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.stereotype.Component;

@Component
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 资源服务器不需要认证
        http.authorizeRequests().antMatchers("/**")
                .permitAll()
                .and()
                // 防止csrf攻击
                .csrf().disable()
                // 禁用缓存
                .headers().cacheControl();
    }
}
