package com.znow.resource.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.*;
import org.springframework.stereotype.Component;


/**
 * 资源服务器
 *
 * @author yuan 2019/7/8 15:38
 */
@Configuration
@EnableResourceServer
@Component
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Autowired
    private TokenStore tokenStore;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                // 开放的路径
                //.antMatchers("/api/**").permitAll()
                .anyRequest()
                .authenticated();
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        /**
         * 默认需要校验token
         */
        //resources.tokenServices(tokenService()).resourceId("oauth2-com.znow.resource")
                //.stateless(true);

        resources.tokenStore(tokenStore).resourceId("oauth2-com.znow.resource")
                .stateless(true);
    }

    /**
     * 资源服务令牌解析服务，需要远程访问认证服务器校验token
     */
    @Bean
    public ResourceServerTokenServices tokenService() {
        // 使用远程服务请求授权服务器校验token,必须指定校验token 的url、client_id，client_secret
        RemoteTokenServices service = new RemoteTokenServices();
        service.setCheckTokenEndpointUrl("http://localhost:8001/oauth/check_token");
        service.setClientId("client_app2");
        service.setClientSecret("123456");
        return service;
    }

}
