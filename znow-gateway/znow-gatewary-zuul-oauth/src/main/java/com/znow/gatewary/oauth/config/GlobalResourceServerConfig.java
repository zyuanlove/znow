package com.znow.gatewary.oauth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;


/**
 * 统一管理资源服务器
 *
 * @author yuan 2019/7/8 15:38
 */
@Configuration
@EnableResourceServer
public class GlobalResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Autowired
    private TokenStore tokenStore;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
            // 开放认证服务器请求
            .antMatchers("/auth/**").permitAll()
    //.antMatchers("/product/**").access("#oauth2.hasScope('ROLE_w')")
                .anyRequest().authenticated();
}

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.tokenStore(tokenStore).resourceId("oauth2-resource")
                .stateless(true);
    }

}
