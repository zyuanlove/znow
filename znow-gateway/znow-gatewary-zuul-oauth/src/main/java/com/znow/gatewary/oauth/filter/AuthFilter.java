package com.znow.gatewary.oauth.filter;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 将认证过用户信息设置到路由跳转服务器请求头中
 */
@Component
public class AuthFilter extends ZuulFilter {

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext ctx = RequestContext.getCurrentContext();
        //从安全上下文中拿 到用户身份对象
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // 如果认证过
        if (authentication != null && !authentication.isAuthenticated()) {
            return null;
        }
        if (!(authentication instanceof OAuth2Authentication)) {
            return null;
        }
        OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) authentication;
        OAuth2Request oAuth2Request = oAuth2Authentication.getOAuth2Request();
        Map<String, String> requestParameters = oAuth2Request.getRequestParameters();

        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

        Map tokenDetail = Maps.newHashMap(requestParameters);
        // 获取所有权限信息
        tokenDetail.put("authorities", getAuthorityList(authorities));
        tokenDetail.put("username", authentication.getPrincipal());

        ctx.addZuulRequestHeader("user-token", JSON.toJSONString(tokenDetail));
        return null;
    }

    private List<String> getAuthorityList(Collection<? extends GrantedAuthority> authorities){
        List<String> authoriyArray = Lists.newArrayList();
        if(CollectionUtils.isNotEmpty(authorities)){
            for (GrantedAuthority item : authorities) {
                if(StringUtils.isEmpty(item.getAuthority())){
                    authoriyArray.add(item.getAuthority());
                }
            }
        }
        return authoriyArray;
    }
}
