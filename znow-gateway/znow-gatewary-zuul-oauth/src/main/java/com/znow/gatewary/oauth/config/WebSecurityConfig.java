package com.znow.gatewary.oauth.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.stereotype.Component;

/**
 * 网关集成oauth2，网关当做资源服务器控制资源
 */
@Component
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 防止csrf攻击
        http.csrf().disable()
                .authorizeRequests()
                // 网关开放所有请求
                .antMatchers("/**").permitAll().and()
                // 不需要创建session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()

                .headers().frameOptions().sameOrigin()
                .cacheControl().and();

    }
}
