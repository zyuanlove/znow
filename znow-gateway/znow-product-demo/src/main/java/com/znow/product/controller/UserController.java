package com.znow.product.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class UserController {

    @RequestMapping("/test")
    //@PreAuthorize("hasAuthority('p1')")
    public String getUser() {
        return "hello";
    }
}
