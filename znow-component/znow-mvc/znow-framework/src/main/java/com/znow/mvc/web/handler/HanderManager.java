package com.znow.mvc.web.handler;

import com.znow.mvc.web.mvc.Controller;
import com.znow.mvc.web.mvc.RequestMapping;
import com.znow.mvc.web.mvc.RequestParam;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;

public class HanderManager {

    public static List<MappingHandler> mappingHandlerList = new ArrayList<>();

    public static void resolveMappingHander(List<Class<?>> classes) {
        if (classes == null || classes.size() <= 0) {

        }

        for (Class<?> item : classes) {
            if (item.isAnnotationPresent(Controller.class)) {
                parseHanderFromController(item);
            }
        }
    }

    /**
     * 获取所有的Controller转换为MapperingHandler设置的集合中
     *
     * @author yuan 2019/7/26 9:39
     */
    private static void parseHanderFromController(Class<?> clz) {
        Method[] methods = clz.getDeclaredMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(RequestMapping.class)) {
                String uri = method.getDeclaredAnnotation(RequestMapping.class).value();
                List<String> paramNameList = new ArrayList<>();

                for (Parameter parameter : method.getParameters()) {
                    if (parameter.isAnnotationPresent(RequestParam.class)) {
                        paramNameList.add(parameter.getDeclaredAnnotation(RequestParam.class).value());
                    }
                }

                String[] params = paramNameList.toArray(new String[paramNameList.size()]);

                MappingHandler mappingHandler = new MappingHandler(uri, method, clz, params);
                HanderManager.mappingHandlerList.add(mappingHandler);
            }

        }
    }
}
