package com.znow.mvc.web.mvc;

import java.lang.annotation.*;

/**
 * 自定义controller注解
 *
 * @author yuan 2019/7/25 9:50
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Controller {
}
