package com.znow.mvc.web.server;

import com.znow.mvc.web.handler.HanderManager;
import com.znow.mvc.web.handler.MappingHandler;

import javax.servlet.*;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

/**
 * 自定义前端控制器
 * <p>
 * 1、  用户发送请求至前端控制器DispatcherServlet。
 * <p>
 * 2、  DispatcherServlet收到请求调用HandlerMapping处理器映射器。
 * <p>
 * 3、  处理器映射器找到具体的处理器(可以根据xml配置、注解进行查找)，生成处理器对象及处理器拦截器(如果有则生成)一并返回给DispatcherServlet。
 * <p>
 * 4、  DispatcherServlet调用HandlerAdapter处理器适配器。
 * <p>
 * 5、  HandlerAdapter经过适配调用具体的处理器(Controller，也叫后端控制器)。
 * <p>
 * 6、  Controller执行完成返回ModelAndView。
 * <p>
 * 7、  HandlerAdapter将controller执行结果ModelAndView返回给DispatcherServlet。
 * <p>
 * 8、  DispatcherServlet将ModelAndView传给ViewReslover视图解析器。
 * <p>
 * 9、  ViewReslover解析后返回具体View。
 * <p>
 * 10、DispatcherServlet根据View进行渲染视图（即将模型数据填充至视图中）。
 * <p>
 * 11、 DispatcherServlet响应用户。
 *
 * @author yuan 2019/7/24 13:10
 */
public class DispatcherServlet implements Servlet {

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {

    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        if (HanderManager.mappingHandlerList.size() > 0) {
            for (MappingHandler handler : HanderManager.mappingHandlerList) {
                try {
                    if (handler.handler(servletRequest, servletResponse)) {
                        return;
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void destroy() {

    }
}
