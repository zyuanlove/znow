package com.znow.mvc.web.controller;

import com.znow.mvc.beans.AutoWired;
import com.znow.mvc.web.mvc.Controller;
import com.znow.mvc.web.mvc.RequestMapping;
import com.znow.mvc.web.mvc.RequestParam;
import com.znow.mvc.web.service.UserService;

@Controller
public class UserController {

    @AutoWired
    private UserService userService;

    @RequestMapping("/user")
    public String getUser(@RequestParam("userId") String userId) {
        System.out.println(userId);
        return userId;
    }

    @RequestMapping("/age")
    public String getUserList(@RequestParam("userId") String userId) {
        System.out.println(userId);
        return userService.getUser(Integer.valueOf(userId));
    }
}
