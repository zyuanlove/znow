package com.znow.mvc.beans;

import com.znow.mvc.web.mvc.Controller;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 自定义beanfactory
 *
 * @author yuan 2019/7/26 16:33
 */
public class BeanFactory {

    private static Map<Class<?>, Object> classTobean = new ConcurrentHashMap<>();

    public static void initBean(List<Class<?>> classList) throws Exception {
        List<Class<?>> toCreate = new ArrayList<>(classList);
        while (toCreate.size() != 0) {
            int remainSize = toCreate.size();
            for (int i = 0; i < toCreate.size(); i++) {
                // 如果没有Bean注解和Controller注解的类被过滤掉
                if (finishCreate(toCreate.get(i))) {
                    toCreate.remove(i);
                }
            }
            // 没有bean注解的类
            if (toCreate.size() == remainSize) {
                throw new Exception("cycle dependency!");
            }
        }
    }

    private static boolean finishCreate(Class<?> cls) throws IllegalAccessException, InstantiationException {
        if (!cls.isAnnotationPresent(Bean.class) && !cls.isAnnotationPresent(Controller.class)) {
            return true;
        }

        Object bean = cls.newInstance();
        for (Field field : cls.getDeclaredFields()) {
            if (field.isAnnotationPresent(AutoWired.class)) {
                Class<?> fieldType = field.getType();
                Object reliantBean = BeanFactory.getBean(fieldType);
                if (reliantBean == null) {
                    return false;
                }
                field.setAccessible(true);
                field.set(bean, reliantBean);
            }
        }
        classTobean.put(cls, bean);
        return true;
    }

    public static Object getBean(Class<?> cls) {
        return classTobean.get(cls);
    }

}
