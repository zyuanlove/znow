package com.znow.mvc.starter;


import com.znow.mvc.beans.BeanFactory;
import com.znow.mvc.core.ClassScanner;
import com.znow.mvc.web.handler.HanderManager;
import com.znow.mvc.web.server.CustomTomcatServer;
import org.apache.catalina.LifecycleException;

import java.util.List;

/**
 * mvc启动类
 *
 * @author yuan 2019/7/24 9:41
 */
public class Application {

    public static void main(String[] args) throws Exception {
        System.out.println("启动-------------------tomcat");
        CustomTomcatServer customTomcatServer = new CustomTomcatServer(args);

        List<Class<?>> classList = ClassScanner.scanClasses("com.znow.mvc");

        BeanFactory.initBean(classList);

        HanderManager.resolveMappingHander(classList);

        classList.forEach(item -> System.out.println(item.getName()));
        try {
            customTomcatServer.startServer();
        } catch (LifecycleException e) {
            e.printStackTrace();
        }
    }
}
