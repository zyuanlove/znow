package com.znow.mvc.web.handler;

import com.znow.mvc.beans.BeanFactory;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MappingHandler {

    private String uri;

    private Method method;

    private Class<?> controllerClass;

    private String[] args;

    public MappingHandler(String uri, Method method, Class<?> controllerClass, String[] args) {
        this.uri = uri;
        this.method = method;
        this.controllerClass = controllerClass;
        this.args = args;
    }

    /**
     * 前端控制器执行逻辑
     */
    public boolean handler(ServletRequest request, ServletResponse response) throws IllegalAccessException, InstantiationException, InvocationTargetException, IOException {
        String requestUri = ((HttpServletRequest) request).getRequestURI();
        if (requestUri.equals(uri)) {
            Object[] paramters = new Object[args.length];

            for (int i = 0; i < args.length; i++) {
                paramters[i] = request.getParameter(args[i]);
            }

            Object ctl = BeanFactory.getBean(controllerClass);
            Object methodRes = method.invoke(ctl, paramters);
            if(methodRes != null){
                response.getWriter().println(methodRes.toString());
            }
            return true;
        }
        return false;
    }


}
