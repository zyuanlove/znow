package com.znow.mvc.web.server;


import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;

/**
 * 自定义tomcat服务
 *
 * @author yuan 2019/7/24 10:10
 */
public class CustomTomcatServer {

    /**
     * tomcat对象
     */
    private Tomcat tomcat;

    private String[] args;

    public CustomTomcatServer(String[] args){
        this.args = args;
    }

    public void startServer() throws LifecycleException {
        tomcat = new Tomcat();
        tomcat.setHostname("localhost");
        tomcat.setPort(8080);
        tomcat.start();

        Context context = new StandardContext();
        context.setPath("");
        context.addLifecycleListener(new Tomcat.FixContextListener());

        DispatcherServlet dispatcherServlet = new DispatcherServlet();
        // 添加servert
        Tomcat.addServlet(context,"dispatcherServlet",dispatcherServlet).setAsyncSupported(true);

        context.addServletMappingDecoded("/","dispatcherServlet");

        tomcat.getHost().addChild(context);

        Thread awaitThread = new Thread("tomcat_await_thread"){
            @Override
            public void run() {
                CustomTomcatServer.this.tomcat.getServer().await();
            }
        };

        awaitThread.setDaemon(false);
        awaitThread.start();

    }


}
