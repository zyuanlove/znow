package com.znow.common.utils.httpclient;

import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

/**
 * 发送httpclient请求组件
 *
 * @author yuan 2020/5/9 16:17
 */
@Component
public class HttpClientUtils {

    private static final Logger logger = LoggerFactory.getLogger(HttpClientUtils.class);

    private CloseableHttpClient httpClient;

    private RequestConfig requestConfig;

    private static final String UTF8 = "UTF-8";

    private static final String EMPTY = "";

    @Autowired
    public HttpClientUtils(CloseableHttpClient httpClient, RequestConfig requestConfig) {
        this.httpClient = httpClient;
        this.requestConfig = requestConfig;
    }

    /**
     * httpclient发送get请求不带任何参数
     *
     * @param url
     */
    public String doGet(String url) {
        return this.executeGet(url, null, null);
    }

    public String doGet(String url, Map<String, Object> paramMap) {
        return this.executeGet(url, null, paramMap);
    }

    public String doGet(String url, Map<String, Object> headerMap, Map<String, Object> paramMap) {
        return this.executeGet(url, headerMap, paramMap);
    }

    /**
     * httpclient发送get请求
     *
     * @param url       链接地址
     * @param headerMap 请求头
     * @param paramMap  参数
     * @return 响应数据
     */
    private String executeGet(String url, Map<String, Object> headerMap, Map<String, Object> paramMap) {
        String result = EMPTY;
        CloseableHttpResponse response = null;
        try {
            URIBuilder uriBuilder = new URIBuilder(url);
            // 拼装参数
            if (!isMapEmpty(paramMap)) {
                for (Map.Entry<String, Object> entry : paramMap.entrySet()) {
                    uriBuilder.addParameter(entry.getKey(), entry.getValue().toString());
                }
            }
            String urlParam = uriBuilder.build().toString();
            HttpGet httpGet = new HttpGet(urlParam);
            // 增加请求头参数
            this.addHeader(headerMap, httpGet);
            response = httpClient.execute(httpGet);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                // 把数据转换成字符串
                result = EntityUtils.toString(response.getEntity(), UTF8);
            }
            logger.info("httpclient请求地址：[{}]；响应状态：[{}]", urlParam, response.getStatusLine().getStatusCode());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            this.closeResponse(response);
        }
        return result;
    }

    private static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    private static boolean isMapEmpty(Map map) {
        return map == null || map.isEmpty();
    }

    /**
     * 请求头增加参数
     */
    private void addHeader(Map<String, Object> headerMap, HttpRequestBase httpRequestBase) {
        if (!isMapEmpty(headerMap)) {
            for (Map.Entry<String, Object> entry : headerMap.entrySet()) {
                httpRequestBase.addHeader(entry.getKey(), entry.getValue().toString());
            }
        }
    }

    /**
     * 关闭httpResponse
     */
    private void closeResponse(CloseableHttpResponse response) {
        try {
            if (response != null) {
                response.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}