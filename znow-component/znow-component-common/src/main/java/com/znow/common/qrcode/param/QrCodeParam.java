package com.znow.common.qrcode.param;

import com.znow.common.qrcode.config.ImageTypeEnum;
import com.znow.common.utils.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class QrCodeParam {
    /**
     * 二维码最大容量
     */
    private static final int MAX_LENGTH = 1817;

    /**
     * 二维码图片
     */
    private File imageFile;
    /**
     * 二维码图片类型
     */
    private ImageTypeEnum imageTypeEnum;
    /**
     * 二维码内容
     */
    private String message;
    /**
     * 二维码宽
     */
    private int width;
    /**
     * 二维码高
     */
    private int height;

    /**
     * 二维码图片背景色，默认白色
     */
    private int backgroundColor;
    /**
     * 二维码图片颜色，默认黑色
     */
    private int imageColor;
    /**
     * 带logo图标文件位置,若未设置或路径文件不存在则二维码中不带logo
     */
    private String logoPath;

    /**
     * 是否输出到流
     */
    private FileOutputStream fileOutputStream;

    public QrCodeParam() {
        this.width = 100;
        this.height = 100;
        this.imageTypeEnum = ImageTypeEnum.JPEG;
        this.backgroundColor = Color.WHITE.getRGB();
        this.imageColor = Color.black.getRGB();
    }

    /**
     * 二维码参数实体类构造函数
     *
     * @param imageFilePath 二维码图片存放位置
     * @param message       二维码内容
     * @throws IOException
     */
    public QrCodeParam(String imageFilePath, String message) throws IOException {
        if (StringUtils.isEmpty(imageFilePath)) {
            throw new IllegalArgumentException("二维码存放路径不能为空");
        } else {
            if (!FileUtils.checkAndCreateFile(imageFilePath)) {
                throw new IOException("创建文件夹路径下文件失败");
            } else {
                if (StringUtils.isEmpty(message) || message.length() > MAX_LENGTH) {
                    throw new IllegalArgumentException("二维码内容不能为空或超过最大值");
                } else {
                    initQrCode(new File(imageFilePath), message);
                }
            }
        }
    }

    /**
     * 二维码参数实体类构造函数
     *
     * @param imageFile 二维码图片对象
     * @param message   二维码内容
     * @throws IOException
     */
    public QrCodeParam(File imageFile, String message) throws IOException {
        if (FileUtils.checkAndCreateFile(imageFile)) {
            throw new IOException("创建文件夹路径下文件失败");
        } else {
            if (StringUtils.isEmpty(message) || message.length() > MAX_LENGTH) {
                throw new IllegalArgumentException("二维码内容不能为空或超过最大值");
            } else {
                initQrCode(imageFile, message);
            }
        }
    }

    private void initQrCode(File imageFile, String message) {
        this.imageFile = imageFile;
        this.message = message;
        this.width = 100;
        this.height = 100;
        this.imageTypeEnum = ImageTypeEnum.JPEG;
        this.backgroundColor = Color.WHITE.getRGB();
        this.imageColor = Color.black.getRGB();
    }

    public void setImageFile(File imageFile) {
        this.imageFile = imageFile;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setFileOutputStream(FileOutputStream fileOutputStream) {
        this.fileOutputStream = fileOutputStream;
    }

    public FileOutputStream getFileOutputStream() {
        return fileOutputStream;
    }

    public File getImageFile() {
        return imageFile;
    }

    public String getMessage() {
        return message;
    }

    public ImageTypeEnum getImageTypeEnum() {
        return imageTypeEnum;
    }

    public void setImageTypeEnum(ImageTypeEnum imageTypeEnum) {
        this.imageTypeEnum = imageTypeEnum;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public int getImageColor() {
        return imageColor;
    }

    public void setImageColor(int imageColor) {
        this.imageColor = imageColor;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) throws IOException {
        File logoFile = new File(logoPath);
        if (!logoFile.exists()) {
            throw new IOException("logo图片不存在");
        } else {
            this.logoPath = logoPath;
        }
    }

}
