package com.znow.common.utils;


import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;


public class NumberUtils {

    private NumberUtils() {
    }

    //private static final Logger LOGGER = LoggerFactory.getLogger(NumberUtils.class);

    private static String MONEY_FORMAT = "###,##0.00";

    private static ThreadLocal<DecimalFormat> MONEY_FORMAT_THREAD_LOCAL = ThreadLocal.withInitial(() -> new DecimalFormat(MONEY_FORMAT));


    /**
     * 格式化<code>BigDecimal</code>
     *
     * @param num 数值
     * @return 格式化结果
     */
    public static String format(BigDecimal num) {
        double val = 0d;
        if (null != num) {
            val = num.setScale(2, RoundingMode.HALF_UP).doubleValue();
        }
        return MONEY_FORMAT_THREAD_LOCAL.get().format(val);
    }

    /**
     * 格式化double类型为字符串
     *
     * @param num 数值
     * @return 格式化结果
     */
    public static String format(double num) {
        return MONEY_FORMAT_THREAD_LOCAL.get().format(num);
    }

    /**
     * 格式化long类型为字符串
     *
     * @param num 数值
     * @return 格式化结果
     */
    public static String format(long num) {
        return MONEY_FORMAT_THREAD_LOCAL.get().format(num);
    }

    /**
     * 格式化int类型为字符串
     *
     * @param num 数值
     * @return 格式化结果
     */
    public static String format(int num) {
        return MONEY_FORMAT_THREAD_LOCAL.get().format(num);
    }

    /**
     * 将字符串解析为整数
     *
     * @param source 源字符串
     * @return 转换结果，如果格式无法解析，将返回0
     */
    public static int parseToInt(String source) {
        try {
            return MONEY_FORMAT_THREAD_LOCAL.get().parse(source).intValue();
        } catch (ParseException e) {
            //LOGGER.debug(e.getMessage());
            return 0;
        }
    }

    /**
     * 将字符串解析为float
     *
     * @param source 源字符串
     * @return 转换结果，如果格式无法解析，将返回0
     */
    public static float parseToFloat(String source) {
        try {
            return MONEY_FORMAT_THREAD_LOCAL.get().parse(source).floatValue();
        } catch (ParseException e) {
            //LOGGER.debug(e.getMessage());
            return 0f;
        }
    }

    /**
     * 将字符串解析为double
     *
     * @param source 源字符串
     * @return 转换结果，如果格式无法解析，将返回0
     */
    public static double parseToDouble(String source) {
        try {
            return MONEY_FORMAT_THREAD_LOCAL.get().parse(source).doubleValue();
        } catch (ParseException e) {
            //LOGGER.debug(e.getMessage());
            return 0d;
        }
    }

    /**
     * 将字符串解析为long
     *
     * @param source 源字符串
     * @return 转换结果，如果格式无法解析，将返回0
     */
    public static long parseToLong(String source) {
        try {
            return MONEY_FORMAT_THREAD_LOCAL.get().parse(source).longValue();
        } catch (ParseException e) {
            //LOGGER.debug(e.getMessage());
            return 0L;
        }
    }

    /**
     * 将字符串解析为BigDecimal
     *
     * @param source 源字符串
     * @return 转换结果，如果格式无法解析，将返回0
     */
    public static BigDecimal parseToDecimal(String source) {
        double val = parseToDouble(source);
        return new BigDecimal(val);
    }

    /**
     * 将BigDecimal转换为中文大写金额单位
     *
     * @param num 数值
     * @return 转换结果
     */
    public static String parseToCNMonetaryUnit(BigDecimal num) {
        if (null == num) {
            num = new BigDecimal(0);
        }
        return number2CNMonetaryUnit(num);
    }

    /**
     * 汉语中数字大写
     */
    private static final String[] CN_UPPER_NUMBER = {"零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"};
    /**
     * 汉语中货币单位大写，这样的设计类似于占位符
     */
    private static final String[] CN_UPPER_MONETRAY_UNIT = {"分", "角", "元", "拾", "佰", "仟", "万", "拾", "佰", "仟", "亿", "拾", "佰", "仟", "兆", "拾", "佰", "仟"};
    /**
     * 特殊字符：整
     */
    private static final String CN_FULL = "整";
    /**
     * 特殊字符：负
     */
    private static final String CN_NEGATIVE = "负";
    /**
     * 金额的精度，默认值为2
     */
    private static final int MONEY_PRECISION = 2;
    /**
     * 特殊字符：零元整
     */
    private static final String CN_ZEOR_FULL = "零元" + CN_FULL;

    /**
     * 把输入的金额转换为汉语中人民币的大写
     *
     * @param numberOfMoney 输入的金额
     * @return 对应的汉语大写
     */
    private static String number2CNMonetaryUnit(BigDecimal numberOfMoney) {
        StringBuilder sb = new StringBuilder();
        // -1, 0, or 1 as the value of this BigDecimal is negative, zero, or
        // positive.
        int signum = numberOfMoney.signum();
        // 零元整的情况
        if (signum == 0) {
            return CN_ZEOR_FULL;
        }
        // 这里会进行金额的四舍五入
        long number = numberOfMoney.movePointRight(MONEY_PRECISION).setScale(0, 4).abs().longValue();
        // 得到小数点后两位值
        long scale = number % 100;
        int numUnit = 0;
        int numIndex = 0;
        boolean getZero = false;
        // 判断最后两位数，一共有四中情况：00 = 0, 01 = 1, 10, 11
        if (!(scale > 0)) {
            numIndex = 2;
            number = number / 100;
            getZero = true;
        }
        if ((scale > 0) && (!(scale % 10 > 0))) {
            numIndex = 1;
            number = number / 10;
            getZero = true;
        }
        int zeroSize = 0;
        while (true) {
            if (number <= 0) {
                break;
            }
            // 每次获取到最后一个数
            numUnit = (int) (number % 10);
            if (numUnit > 0) {
                if ((numIndex == 9) && (zeroSize >= 3)) {
                    sb.insert(0, CN_UPPER_MONETRAY_UNIT[6]);
                }
                if ((numIndex == 13) && (zeroSize >= 3)) {
                    sb.insert(0, CN_UPPER_MONETRAY_UNIT[10]);
                }
                sb.insert(0, CN_UPPER_MONETRAY_UNIT[numIndex]);
                sb.insert(0, CN_UPPER_NUMBER[numUnit]);
                getZero = false;
                zeroSize = 0;
            } else {
                ++zeroSize;
                if (!(getZero)) {
                    sb.insert(0, CN_UPPER_NUMBER[numUnit]);
                }
                if (numIndex == 2) {
                    sb.insert(0, CN_UPPER_MONETRAY_UNIT[numIndex]);
                } else if (((numIndex - 2) % 4 == 0) && (number % 1000 > 0)) {
                    sb.insert(0, CN_UPPER_MONETRAY_UNIT[numIndex]);
                }
                getZero = true;
            }
            // 让number每次都去掉最后一个数
            number = number / 10;
            ++numIndex;
        }
        // 如果signum == -1，则说明输入的数字为负数，就在最前面追加特殊字符：负
        if (signum == -1) {
            sb.insert(0, CN_NEGATIVE);
        }
        // 输入的数字小数点后两位为"00"的情况，则要在最后追加特殊字符：整
        if (!(scale > 0)) {
            sb.append(CN_FULL);
        }
        return sb.toString();
    }

    public static boolean checkNotEmpty(Long value){
        return value != null && value.longValue() >= 0;
    }

    public static boolean checkEmpty(Long value){
        return !checkNotEmpty(value);
    }
}
