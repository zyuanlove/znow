package com.znow.common.utils;

import com.znow.common.constans.BaseEnum;

/**
 * 枚举工具类减少代码量 , 更加简洁美观
 * 但自定义的枚举类必须实现 {@link BaseEnum}接口而且要覆盖其方法
 *
 * @author yuan 2018/7/24 11:15
 */
public class EnumUtils {

    /**
     * 通过枚举类中的code值获取枚举类
     *
     * @param typeClass 枚举类,类必须实现自{@link BaseEnum}
     * @param code      code值
     * @return 检索的枚举对象，不存在时返回null
     */
    public static <T extends BaseEnum> T getEnumByCode(Class<T> typeClass, int code) {
        T target = null;
        for (T labelEnum : typeClass.getEnumConstants()) {
            if (labelEnum.getCode() == code) {
                target = labelEnum;
            }
        }
        return target;
    }

    /**
     * 通过枚举类中的name值获取枚举类
     *
     * @param typeClass 枚举类,类必须实现自{@link BaseEnum}
     * @param name      枚举name
     * @return 检索的枚举对象，不存在时返回null
     */
    public static <T extends BaseEnum> T getEnumByName(Class<T> typeClass, String name) {
        T target = null;
        for (T labelEnum : typeClass.getEnumConstants()) {
            if (labelEnum.getName().equals(name)) {
                target = labelEnum;
            }
        }
        return target;
    }

    /**
     * 通过枚举类中的desc值获取枚举类
     *
     * @param typeClass 枚举类,类必须实现自{@link BaseEnum}
     * @param desc      枚举desc值
     * @return 检索的枚举对象，不存在时返回null
     */
    public static <T extends BaseEnum> T getEnumByDesc(Class<T> typeClass, String desc) {
        T target = null;
        for (T labelEnum : typeClass.getEnumConstants()) {
            if (labelEnum.getDesc().equals(desc)) {
                target = labelEnum;
            }
        }
        return target;
    }

}
