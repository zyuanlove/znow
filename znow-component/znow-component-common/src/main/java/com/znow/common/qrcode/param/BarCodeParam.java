package com.znow.common.qrcode.param;

import com.znow.common.qrcode.config.BarCodeTypeEnum;
import com.znow.common.qrcode.config.ImageTypeEnum;
import com.znow.common.utils.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.krysalis.barcode4j.HumanReadablePlacement;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class BarCodeParam {
    private int DEFAULT_MAX_LENGTH = 12800;

    /**
     * 条形码内容
     */
    private String message;
    /**
     * 条形码图片路径
     */
    private String barCodeImagePath;
    /**
     * 条形码输出流
     */
    private OutputStream barCodeOutputStream;
    /**
     * 条码类型 默认 BarCodeTypeEnum.UPCA
     */
    private BarCodeTypeEnum barCodeTypeEnum;
    /**
     * 图片类型 默认gif
     */
    private ImageTypeEnum barCodeImageType;
    /**
     * 条形码宽度，默认2
     */
    private double width;
    /**
     * 条形码高度，默认10
     */
    private int height;

    /**
     * 文字显示的位置
     * HumanReadablePlacement.HRP_NONE 不显示
     * HumanReadablePlacement.HRP_TOP 显示在顶部
     * HumanReadablePlacement.HRP_BOTTOM 显示在底部
     */
    private HumanReadablePlacement position;

    /**
     * 条形码参数类构造函数
     *
     * @param barCodeImagePath 条形码图片存放路径
     * @param message          条码内容
     * @throws IOException
     */
    public BarCodeParam(String barCodeImagePath, String message) throws IOException {
        if (StringUtils.isEmpty(barCodeImagePath)) {
            throw new IllegalArgumentException("条形码存放路径不能为空");
        } else {
            if (!FileUtils.checkAndCreateFile(barCodeImagePath)) {
                throw new IOException("创建文件夹路径下文件失败");
            } else {
                if (StringUtils.isEmpty(message) || message.length() > DEFAULT_MAX_LENGTH) {
                    throw new IllegalArgumentException("条形码内容不能为空或超过最大值");
                } else {
                    OutputStream outputStream = new FileOutputStream(barCodeImagePath);
                    this.barCodeImagePath = barCodeImagePath;
                    initBean(outputStream, message);
                }
            }
        }
    }

    /**
     * 条形码参数类构造函数
     *
     * @param outputStream 生成条形码存放流
     * @param message      条形码内容
     */
    public BarCodeParam(OutputStream outputStream, String message) {
        if (outputStream == null) {
            throw new IllegalArgumentException("输出流不能为空");
        } else {
            if (StringUtils.isEmpty(message) || message.length() > DEFAULT_MAX_LENGTH) {
                throw new IllegalArgumentException("条形码内容不能为空或超过最大值");
            } else {
                initBean(outputStream, message);
            }
        }
    }

    private void initBean(OutputStream outputStream, String message) {
        width = 2;
        height = 10;
        barCodeImageType = ImageTypeEnum.PNG;
        position = HumanReadablePlacement.HRP_BOTTOM;
        barCodeTypeEnum = BarCodeTypeEnum.BARCODE128;
        this.message = message;
        this.barCodeOutputStream = outputStream;
    }

    public String getMessage() {
        return message;
    }

    public String getBarCodeImagePath() {
        return barCodeImagePath;
    }

    public OutputStream getBarCodeOutputStream() {
        return barCodeOutputStream;
    }

    public BarCodeTypeEnum getBarCodeTypeEnum() {
        return barCodeTypeEnum;
    }

    public void setBarCodeTypeEnum(BarCodeTypeEnum barCodeTypeEnum) {
        if (BarCodeTypeEnum.EAN.equals(barCodeTypeEnum)) {
            if (message.length() < 7 || message.length() > 8) {
                throw new IllegalArgumentException("EAN码内容长度必须7-8个字符");
            }
        } else if (BarCodeTypeEnum.UPCA.equals(barCodeTypeEnum)) {
            if (message.length() < 11 || message.length() > 13) {
                throw new IllegalArgumentException("UCP码内容长度必须11-12个字符");
            }
        }
        this.barCodeTypeEnum = barCodeTypeEnum;
    }

    public ImageTypeEnum getBarCodeImageType() {
        return barCodeImageType;
    }

    public void setBarCodeImageType(ImageTypeEnum barCodeImageType) {
        this.barCodeImageType = barCodeImageType;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public HumanReadablePlacement getPosition() {
        return position;
    }

    public void setPosition(HumanReadablePlacement position) {
        this.position = position;
    }
}
