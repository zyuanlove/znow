package com.znow.common.qrcode.config;

/**
 * 生成文件类型枚举类
 *
 * @author yuan 2019/8/2 15:10
 */
public enum ImageTypeEnum {

    /**
     * GIF
     */
    GIF("image/gif"),
    BMP("image/bmp"),
    PNG("image/x-png"),
    JPEG("image/jpeg"),
    TIFF("image/tiff"),
    EPS("image/x-eps");

    private String value;

    private ImageTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
