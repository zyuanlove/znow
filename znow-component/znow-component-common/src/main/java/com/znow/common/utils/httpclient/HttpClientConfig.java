package com.znow.common.utils.httpclient;


import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class HttpClientConfig {

    /**
     * 最大连接数
     */
    @Value("${http.max.total}")
    private int maxTotal = 800;

    @Value("${http.default.max.perRoute}")
    private int defaultMaxPerRoute = 80;

    @Value("${http.validate.after.inactivity}")
    private int validateAfterInactivity = 1000;

    @Value("${http.connection.request.timeout}")
    private int connectionRequestTimeout = 5000;

    @Value("${http.connection.timeout}")
    private int connectTimeout = 10000;

    /**
     * 数据传输最长时间
     */
    @Value("${http.socket.timeout}")
    private int socketTimeout = 20000;

    @Value("${waitTime}")
    private int waitTime = 30000;

    @Value("${idleConTime}")
    private int idleConTime = 3;

    @Value("${retryCount}")
    private int retryCount = 3;

    @Bean
    public PoolingHttpClientConnectionManager httpClientConnectionManager() {
        PoolingHttpClientConnectionManager poolmanager = new PoolingHttpClientConnectionManager();
        poolmanager.setMaxTotal(maxTotal);
        poolmanager.setDefaultMaxPerRoute(defaultMaxPerRoute);
        poolmanager.setValidateAfterInactivity(validateAfterInactivity);
        return poolmanager;
    }

    @Bean
    public CloseableHttpClient httpClient() {
        PoolingHttpClientConnectionManager httpClientConnectionManager = this.httpClientConnectionManager();
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create().setConnectionManager(httpClientConnectionManager);

        httpClientBuilder.setKeepAliveStrategy((response, context) -> {
            HeaderElementIterator iterator = new BasicHeaderElementIterator(response.headerIterator(HTTP.CONN_KEEP_ALIVE));
            while (iterator.hasNext()) {
                HeaderElement headerElement = iterator.nextElement();
                String param = headerElement.getName();
                String value = headerElement.getValue();
                if (null != value && param.equalsIgnoreCase("timeout")) {
                    return Long.parseLong(value) * 1000;
                }
            }
            return 30 * 1000;
        });

        httpClientBuilder.setRetryHandler(new DefaultHttpRequestRetryHandler(retryCount, false));
        return httpClientBuilder.build();
    }

    @Bean
    public RequestConfig createRequestConfig() {
        return RequestConfig.custom()
                // 从连接池中取连接的超时时间
                .setConnectionRequestTimeout(connectionRequestTimeout)
                // 连接超时时间
                .setConnectTimeout(connectTimeout)
                // 请求超时时间
                .setSocketTimeout(socketTimeout)
                .build();
    }

    @Bean
    public HttpConnectionEvictor createIdleConnectionEvictor(PoolingHttpClientConnectionManager poolManager) {
        HttpConnectionEvictor httpConnectionEvictor = new HttpConnectionEvictor(poolManager, waitTime, idleConTime);
        return httpConnectionEvictor;
    }

}