package com.znow.common.qrcode;

import com.google.zxing.*;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.znow.common.qrcode.param.BarCodeParam;
import com.znow.common.qrcode.param.QrCodeParam;
import org.apache.commons.lang3.StringUtils;
import org.apache.fop.util.UnitConv;
import org.krysalis.barcode4j.impl.AbstractBarcodeBean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Hashtable;

/**
 * 二维码、条形码创建工具
 */
public class CodeCreaterFactory {

    private static Logger logger = LoggerFactory.getLogger(CodeCreaterFactory.class);

    /**
     * 图片像素
     */
    private static final int DPI = 150;

    /**
     * 创建条形码
     */
    public static void createBarCode(BarCodeParam barCodeParam) {

        AbstractBarcodeBean bean = barCodeParam.getBarCodeTypeEnum().getObject();
        BitmapCanvasProvider canvas = new BitmapCanvasProvider(barCodeParam.getBarCodeOutputStream(), barCodeParam.getBarCodeImageType().getValue(),
                DPI, BufferedImage.TYPE_BYTE_BINARY, false, 0);
        // 设置条码参数
        bean.setBarHeight(barCodeParam.getHeight());
        //bean.setModuleWidth(barCodeParam.getWidth());
        bean.doQuietZone(false);
        bean.setModuleWidth(UnitConv.in2mm(barCodeParam.getWidth() / DPI));

        // 生成条码
        bean.generateBarcode(canvas, barCodeParam.getMessage());

        try {
            canvas.finish();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * 创建二维码
     *
     * @param qrCodeParam 二维码参数
     */
    public static void createQrCode(QrCodeParam qrCodeParam) {
        Writer writer = new MultiFormatWriter();
        Hashtable<EncodeHintType, Object> hints = new Hashtable<>();

        // 设定编码
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        // 指定纠错等级
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        try {
            // 根据参数生成二维码
            BitMatrix bitMatrix = writer.encode(qrCodeParam.getMessage(), BarcodeFormat.QR_CODE, qrCodeParam.getWidth(), qrCodeParam.getHeight(), hints);

            if (StringUtils.isEmpty(qrCodeParam.getLogoPath())) {
                // logo图片路径是否为空,若为空则生成不带logo的二维码
                writerToFileWithOutLogo(bitMatrix, qrCodeParam);
            } else {
                writerToFileWithLogo(bitMatrix, qrCodeParam);
            }
        } catch (WriterException | IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * 将二维码存到文件中
     *
     * @param matrix      二维码
     * @param qrCodeParam 二维码参数对象
     * @throws IOException 生成图片失败
     */
    private static void writerToFileWithOutLogo(BitMatrix matrix, QrCodeParam qrCodeParam) throws IOException {
        BufferedImage image = toBufferedImage(matrix, qrCodeParam);
        try {
            if (null != qrCodeParam.getFileOutputStream()) {
                ImageIO.write(image, qrCodeParam.getImageTypeEnum().name(), qrCodeParam.getFileOutputStream());
            } else {
                ImageIO.write(image, qrCodeParam.getImageTypeEnum().name(), qrCodeParam.getImageFile());
            }
        } catch (IOException e) {
            throw new IOException("生成图片失败");
        }
    }

    /**
     * 将带logo的二维码存到文件中
     *
     * @param matrix      二维码
     * @param qrCodeParam 二维码参数对象
     * @throws IOException 生成图片失败
     */
    private static void writerToFileWithLogo(BitMatrix matrix, QrCodeParam qrCodeParam) throws IOException {
        BufferedImage image = toBufferedImage(matrix, qrCodeParam);
        Graphics2D gs = image.createGraphics();

        // 载入logo
        int width = qrCodeParam.getWidth() / 4 > 0 ? qrCodeParam.getWidth() / 4 : 1;
        int height = qrCodeParam.getHeight() / 4 > 0 ? qrCodeParam.getHeight() / 4 : 1;
        int positionX = qrCodeParam.getWidth() * 3 / 8;
        int positionY = qrCodeParam.getHeight() * 3 / 8;
        Image img = scale(qrCodeParam.getLogoPath(), width, height);
        gs.drawImage(img, positionX, positionY, null);
        gs.dispose();
        img.flush();
        if (null != qrCodeParam.getFileOutputStream()) {
            ImageIO.write(image, qrCodeParam.getImageTypeEnum().name(), qrCodeParam.getFileOutputStream());
        } else {
            ImageIO.write(image, qrCodeParam.getImageTypeEnum().name(), qrCodeParam.getImageFile());
        }
    }

    /**
     * 将二维码转换成图片流
     *
     * @param matrix 二维码
     * @param bean   生成二维码的参数信息
     * @return 图片文件流
     */
    private static BufferedImage toBufferedImage(BitMatrix matrix, QrCodeParam bean) {
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                image.setRGB(x, y, matrix.get(x, y) ? bean.getImageColor() : bean.getBackgroundColor());
            }
        }
        return image;
    }

    private static BufferedImage scale(String srcImageFile, int height, int width) throws IOException {
        double ratio = 0.0; // 缩放比例
        File file = new File(srcImageFile);
        BufferedImage srcImage = ImageIO.read(file);
        Image destImage = srcImage.getScaledInstance(width, height, BufferedImage.SCALE_SMOOTH);
        // 计算比例
        if ((srcImage.getHeight() > height) || (srcImage.getWidth() > width)) {
            if (srcImage.getHeight() > srcImage.getWidth()) {
                ratio = (new Integer(height)).doubleValue() / srcImage.getHeight();
            } else {
                ratio = (new Integer(width)).doubleValue() / srcImage.getWidth();
            }
            AffineTransformOp op = new AffineTransformOp(AffineTransform.getScaleInstance(ratio, ratio), null);
            destImage = op.filter(srcImage, null);
        }
        // 补白
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphic = image.createGraphics();
        graphic.setColor(Color.white);
        graphic.fillRect(0, 0, width, height);
        if (width == destImage.getWidth(null)) {
            graphic.drawImage(destImage, 0, (height - destImage.getHeight(null)) / 2, destImage.getWidth(null), destImage.getHeight(null),
                    Color.white, null);
        } else {
            graphic.drawImage(destImage, (width - destImage.getWidth(null)) / 2, 0, destImage.getWidth(null), destImage.getHeight(null),
                    Color.white, null);
        }

        graphic.dispose();
        destImage = image;
        return (BufferedImage) destImage;
    }

    public static void main(String[] args) throws FileNotFoundException {
        //根据路径生成二维码
        QrCodeParam param = new QrCodeParam();
        param.setHeight(300);
        param.setWidth(300);

        FileOutputStream fileOutputStream = new FileOutputStream("d://123.jpeg");
        param.setFileOutputStream(fileOutputStream);
        param.setMessage("http://www.baidu.com");

        createQrCode(param);
    }
}
