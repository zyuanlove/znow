package com.znow.common.utils.date;

import java.util.Date;


/**
 * 日期区间对象
 */
public class IntervalMap {

    protected IntervalMap(Date startTime, Date endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    private Date startTime;

    private Date endTime;

    public Date getStartTime() {
        return startTime;
    }

    protected void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    protected void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(super.toString()).append("[");
        str.append("startTime:").append(DateUtils.formatDateTime(startTime)).append(",");
        str.append("endTime:").append(DateUtils.formatDateTime(endTime));
        str.append("]");
        return str.toString();
    }
}