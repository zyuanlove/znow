package com.znow.common.utils;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 文件路径与文件信息获取工具类
 */
public class FileUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);

    private FileUtils() {

    }

    // ==========================================================================
    // 规格化路径。
    // ==========================================================================

    /**
     * 规格化绝对路径。
     * <p>
     * 该方法返回以“<code>/</code>”开始的绝对路径。转换规则如下：
     * </p>
     * <ol>
     * <li>路径为空，则返回<code>""</code>。</li>
     * <li>将所有backslash("\\")转化成slash("/")。</li>
     * <li>去除重复的"/"或"\\"。</li>
     * <li>去除"."，如果发现".."，则向上朔一级目录。</li>
     * <li>保留路径末尾的"/"（如果有的话，除了空路径）。</li>
     * <li>对于绝对路径，如果".."上朔的路径超过了根目录，则看作非法路径，抛出异常。</li>
     * </ol>
     *
     * @param path 要规格化的路径
     * @return 规格化后的路径
     * @throws IllegalArgumentException 如果路径非法
     */
    public static String normalizeAbsolutePath(String path) throws IllegalArgumentException {
        return normalizePath(path, true, false, false);
    }

    /**
     * 规格化绝对路径。
     * <p>
     * 该方法返回以“<code>/</code>”开始的绝对路径。转换规则如下：
     * </p>
     * <ol>
     * <li>路径为空，则返回<code>""</code>。</li>
     * <li>将所有backslash("\\")转化成slash("/")。</li>
     * <li>去除重复的"/"或"\\"。</li>
     * <li>去除"."，如果发现".."，则向上朔一级目录。</li>
     * <li>保留路径末尾的"/"（如果有的话，除了空路径和强制指定<code>removeTrailingSlash==true</code>）。</li>
     * <li>对于绝对路径，如果".."上朔的路径超过了根目录，则看作非法路径，抛出异常。</li>
     * </ol>
     *
     * @param path                要规格化的路径
     * @param removeTrailingSlash 是否强制移除末尾的<code>"/"</code>
     * @return 规格化后的路径
     * @throws IllegalArgumentException 如果路径非法
     */
    public static String normalizeAbsolutePath(String path, boolean removeTrailingSlash) throws IllegalArgumentException {
        return normalizePath(path, true, false, removeTrailingSlash);
    }

    /**
     * 规格化相对路径。
     * <p>
     * 该方法返回不以“<code>/</code>”开始的相对路径。转换规则如下：
     * </p>
     * <ol>
     * <li>路径为空，则返回<code>""</code>。</li>
     * <li>将所有backslash("\\")转化成slash("/")。</li>
     * <li>去除重复的"/"或"\\"。</li>
     * <li>去除"."，如果发现".."，则向上朔一级目录。</li>
     * <li>空相对路径返回""。</li>
     * <li>保留路径末尾的"/"（如果有的话，除了空路径）。</li>
     * </ol>
     *
     * @param path 要规格化的路径
     * @return 规格化后的路径
     * @throws IllegalArgumentException 如果路径非法
     */
    public static String normalizeRelativePath(String path) throws IllegalArgumentException {
        return normalizePath(path, false, true, false);
    }

    /**
     * 规格化相对路径。
     * <p>
     * 该方法返回不以“<code>/</code>”开始的相对路径。转换规则如下：
     * </p>
     * <ol>
     * <li>路径为空，则返回<code>""</code>。</li>
     * <li>将所有backslash("\\")转化成slash("/")。</li>
     * <li>去除重复的"/"或"\\"。</li>
     * <li>去除"."，如果发现".."，则向上朔一级目录。</li>
     * <li>空相对路径返回""。</li>
     * <li>保留路径末尾的"/"（如果有的话，除了空路径和强制指定<code>removeTrailingSlash==true</code>）。</li>
     * </ol>
     *
     * @param path                要规格化的路径
     * @param removeTrailingSlash 是否强制移除末尾的<code>"/"</code>
     * @return 规格化后的路径
     * @throws IllegalArgumentException 如果路径非法
     */
    public static String normalizeRelativePath(String path, boolean removeTrailingSlash) throws IllegalArgumentException {
        return normalizePath(path, false, true, removeTrailingSlash);
    }

    /**
     * 规格化路径。规则如下：
     * <ol>
     * <li>路径为空，则返回<code>""</code>。</li>
     * <li>将所有backslash("\\")转化成slash("/")。</li>
     * <li>去除重复的"/"或"\\"。</li>
     * <li>去除"."，如果发现".."，则向上朔一级目录。</li>
     * <li>空绝对路径返回"/"，空相对路径返回""。</li>
     * <li>保留路径末尾的"/"（如果有的话，除了空路径）。</li>
     * <li>对于绝对路径，如果".."上朔的路径超过了根目录，则看作非法路径，抛出异常。</li>
     * </ol>
     *
     * @param path 要规格化的路径
     * @return 规格化后的路径
     * @throws IllegalArgumentException 如果路径非法
     */
    public static String normalizePath(String path) throws IllegalArgumentException {
        return normalizePath(path, false, false, false);
    }

    /**
     * 规格化路径。规则如下：
     * <ol>
     * <li>路径为空，则返回<code>""</code>。</li>
     * <li>将所有backslash("\\")转化成slash("/")。</li>
     * <li>去除重复的"/"或"\\"。</li>
     * <li>去除"."，如果发现".."，则向上朔一级目录。</li>
     * <li>空绝对路径返回"/"，空相对路径返回""。</li>
     * <li>保留路径末尾的"/"（如果有的话，除了空路径和强制指定<code>removeTrailingSlash==true</code>）。</li>
     * <li>对于绝对路径，如果".."上朔的路径超过了根目录，则看作非法路径，抛出异常。</li>
     * </ol>
     *
     * @param path                要规格化的路径
     * @param removeTrailingSlash 是否强制移除末尾的<code>"/"</code>
     * @return 规格化后的路径
     * @throws IllegalArgumentException 如果路径非法
     */
    public static String normalizePath(String path, boolean removeTrailingSlash) throws IllegalArgumentException {
        return normalizePath(path, false, false, removeTrailingSlash);
    }

    private static String normalizePath(String path, boolean forceAbsolute, boolean forceRelative,
                                        boolean removeTrailingSlash) throws IllegalArgumentException {
        char[] pathChars = StringUtils.trimToEmpty(path).toCharArray();
        int length = pathChars.length;

        // 检查绝对路径，以及path尾部的"/"
        boolean startsWithSlash = false;
        boolean endsWithSlash = false;

        if (length > 0) {
            char firstChar = pathChars[0];
            char lastChar = pathChars[length - 1];

            startsWithSlash = firstChar == '/' || firstChar == '\\';
            endsWithSlash = lastChar == '/' || lastChar == '\\';
        }

        StringBuilder buf = new StringBuilder(length);
        boolean isAbsolutePath = forceAbsolute || !forceRelative && startsWithSlash;
        int index = startsWithSlash ? 0 : -1;
        int level = 0;

        if (isAbsolutePath) {
            buf.append("/");
        }

        while (index < length) {
            // 跳到第一个非slash字符，或末尾
            index = indexOfSlash(pathChars, index + 1, false);

            if (index == length) {
                break;
            }

            // 取得下一个slash index，或末尾
            int nextSlashIndex = indexOfSlash(pathChars, index, true);

            String element = new String(pathChars, index, nextSlashIndex - index);
            index = nextSlashIndex;

            // 忽略"."
            if (".".equals(element)) {
                continue;
            }

            // 回朔".."
            if ("..".equals(element)) {
                if (level == 0) {
                    // 如果是绝对路径，../试图越过最上层目录，这是不可能的，
                    // 抛出路径非法的异常。
                    if (isAbsolutePath) {
                        throw new IllegalArgumentException(path);
                    } else {
                        buf.append("../");
                    }
                } else {
                    buf.setLength(pathChars[--level]);
                }

                continue;
            }

            // 添加到path
            pathChars[level++] = (char) buf.length(); // 将已经读过的chars空间用于记录指定level的index
            buf.append(element).append('/');
        }

        // 除去最后的"/"
        if (buf.length() > 0) {
            if (!endsWithSlash || removeTrailingSlash) {
                buf.setLength(buf.length() - 1);
            }
        }

        return buf.toString();
    }

    private static int indexOfSlash(char[] chars, int beginIndex, boolean slash) {
        int i = beginIndex;

        for (; i < chars.length; i++) {
            char ch = chars[i];

            if (slash) {
                if (ch == '/' || ch == '\\') {
                    break; // if a slash
                }
            } else {
                if (ch != '/' && ch != '\\') {
                    break; // if not a slash
                }
            }
        }

        return i;
    }

    // ==========================================================================
    // 取得基于指定basedir规格化路径。
    // ==========================================================================

    /**
     * 如果指定路径已经是绝对路径，则规格化后直接返回之，否则取得基于指定basedir的规格化路径。
     *
     * @param basedir 根目录，如果<code>path</code>为相对路径，表示基于此目录
     * @param path    要检查的路径
     * @return 规格化的绝对路径
     * @throws IllegalArgumentException 如果路径非法
     */
    public static String getAbsolutePathBasedOn(String basedir, String path) throws IllegalArgumentException {
        // 如果path为绝对路径，则规格化后返回
        boolean isAbsolutePath = false;

        path = StringUtils.trimToEmpty(path);

        if (path.length() > 0) {
            char firstChar = path.charAt(0);
            isAbsolutePath = firstChar == '/' || firstChar == '\\';
        }

        if (!isAbsolutePath) {
            // 如果path为相对路径，将它和basedir合并。
            if (path.length() > 0) {
                path = StringUtils.trimToEmpty(basedir) + "/" + path;
            } else {
                path = StringUtils.trimToEmpty(basedir);
            }
        }

        return normalizeAbsolutePath(path);
    }

    /**
     * 取得和系统相关的绝对路径。
     *
     * @throws IllegalArgumentException 如果basedir不是绝对路径
     */
    public static String getSystemDependentAbsolutePathBasedOn(String basedir, String path) {
        path = StringUtils.trimToEmpty(path);

        boolean endsWithSlash = path.endsWith("/") || path.endsWith("\\");

        File pathFile = new File(path);

        if (pathFile.isAbsolute()) {
            // 如果path已经是绝对路径了，则直接返回之。
            path = pathFile.getAbsolutePath();
        } else {
            // 否则以basedir为基本路径。
            // 下面确保basedir本身为绝对路径。
            basedir = StringUtils.trimToEmpty(basedir);

            File baseFile = new File(basedir);

            if (baseFile.isAbsolute()) {
                path = new File(baseFile, path).getAbsolutePath();
            } else {
                throw new IllegalArgumentException("Basedir is not absolute path: " + basedir);
            }
        }

        if (endsWithSlash) {
            path = path + '/';
        }

        return normalizePath(path);
    }

    // ==========================================================================
    // 取得相对于指定basedir相对路径。
    // ==========================================================================

    /**
     * 取得相对于指定根目录的相对路径。
     *
     * @param basedir 根目录
     * @param path    要计算的路径
     * @return 如果<code>path</code>和<code>basedir</code>是兼容的，则返回相对于
     * <code>basedir</code>的相对路径，否则返回<code>path</code>本身。
     * @throws IllegalArgumentException 如果路径非法
     */
    public static String getRelativePath(String basedir, String path) throws IllegalArgumentException {
        // 取得规格化的basedir，确保其为绝对路径
        basedir = normalizeAbsolutePath(basedir);

        // 取得规格化的path
        path = getAbsolutePathBasedOn(basedir, path);

        // 保留path尾部的"/"
        boolean endsWithSlash = path.endsWith("/");

        // 按"/"分隔basedir和path
        String[] baseParts = StringUtils.split(basedir, '/');
        String[] parts = StringUtils.split(path, '/');
        StringBuilder buf = new StringBuilder();
        int i = 0;

        while (i < baseParts.length && i < parts.length && baseParts[i].equals(parts[i])) {
            i++;
        }

        if (i < baseParts.length && i < parts.length) {
            for (int j = i; j < baseParts.length; j++) {
                buf.append("..").append('/');
            }
        }

        for (; i < parts.length; i++) {
            buf.append(parts[i]);

            if (i < parts.length - 1) {
                buf.append('/');
            }
        }

        if (endsWithSlash && buf.length() > 0 && buf.charAt(buf.length() - 1) != '/') {
            buf.append('/');
        }

        return buf.toString();
    }

    // ==========================================================================
    // 取得文件名后缀。
    // ==========================================================================

    /**
     * 取得文件路径的后缀。
     * <ul>
     * <li>未指定文件名 - 返回<code>null</code>。</li>
     * <li>文件名没有后缀 - 返回<code>null</code>。</li>
     * </ul>
     */
    public static String getExtension(String fileName) {
        return getExtension(fileName, null, false);
    }

    /**
     * 取得文件路径的后缀。
     * <ul>
     * <li>未指定文件名 - 返回<code>null</code>。</li>
     * <li>文件名没有后缀 - 返回<code>null</code>。</li>
     * </ul>
     */
    public static String getExtension(String fileName, boolean toLowerCase) {
        return getExtension(fileName, null, toLowerCase);
    }

    /**
     * 取得文件路径的后缀。
     * <ul>
     * <li>未指定文件名 - 返回<code>null</code>。</li>
     * <li>文件名没有后缀 - 返回指定字符串<code>nullExt</code>。</li>
     * </ul>
     */
    public static String getExtension(String fileName, String nullExt) {
        return getExtension(fileName, nullExt, false);
    }

    /**
     * 取得文件路径的后缀。
     * <ul>
     * <li>未指定文件名 - 返回<code>null</code>。</li>
     * <li>文件名没有后缀 - 返回指定字符串<code>nullExt</code>。</li>
     * </ul>
     */
    public static String getExtension(String fileName, String nullExt, boolean toLowerCase) {
        fileName = StringUtils.trimToNull(fileName);

        if (fileName == null) {
            return null;
        }

        fileName = fileName.replace('\\', '/');
        fileName = fileName.substring(fileName.lastIndexOf("/") + 1);

        int index = fileName.lastIndexOf(".");
        String ext = null;

        if (index >= 0) {
            ext = StringUtils.trimToNull(fileName.substring(index + 1));
        }

        if (ext == null) {
            return nullExt;
        } else {
            return toLowerCase ? ext.toLowerCase() : ext;
        }
    }

    /**
     * 取得指定路径的名称和后缀。
     *
     * @param path 路径
     * @return 路径和后缀
     */
    public static FileNameAndExtension getFileNameAndExtension(String path) {
        return getFileNameAndExtension(path, false);
    }

    /**
     * 取得指定路径的名称和后缀。
     *
     * @param path 路径
     * @return 路径和后缀
     */
    public static FileNameAndExtension getFileNameAndExtension(String path, boolean extensionToLowerCase) {
        path = StringUtils.trimToEmpty(path);

        String fileName = path;
        String extension = null;

        if (!StringUtils.isEmpty(path)) {
            // 如果找到后缀，则index >= 0，且extension != null（除非name以.结尾）
            int index = path.lastIndexOf('.');

            if (index >= 0) {
                extension = StringUtils.trimToNull(StringUtils.substring(path, index + 1));

                if (!StringUtils.containsNone(extension, "/\\")) {
                    extension = null;
                    index = -1;
                }
            }

            if (index >= 0) {
                fileName = StringUtils.substring(path, 0, index);
            }
        }

        return new FileNameAndExtension(fileName, extension, extensionToLowerCase);
    }

    /**
     * 规格化文件名后缀。
     * <ul>
     * <li>除去两边空白。</li>
     * <li>转成小写。</li>
     * <li>除去开头的“<code>.</code>”。</li>
     * <li>对空白的后缀，返回<code>null</code>。</li>
     * </ul>
     */
    public static String normalizeExtension(String ext) {
        ext = StringUtils.trimToNull(ext);

        if (ext != null) {
            ext = ext.toLowerCase();

            if (ext.startsWith(".")) {
                ext = StringUtils.trimToNull(ext.substring(1));
            }
        }

        return ext;
    }

    private static final Pattern schemePrefixPattern = Pattern.compile(
            "(file:/*[a-z]:)|(\\w+://.+?/)|((jar|zip):.+!/)|(\\w+:)", Pattern.CASE_INSENSITIVE);

    /**
     * 根据指定url和相对路径，计算出相对路径所对应的完整url。类似于<code>URI.resolve()</code>
     * 方法，然后后者不能正确处理jar类型的URL。
     */
    public static String resolve(String url, String relativePath) {
        url = StringUtils.trimToEmpty(url);

        Matcher m = schemePrefixPattern.matcher(url);
        int index = 0;

        if (m.find()) {
            index = m.end();

            if (url.charAt(index - 1) == '/') {
                index--;
            }
        }

        return url.substring(0, index) + normalizeAbsolutePath(url.substring(index) + "/../" + relativePath);
    }

    public static class FileNameAndExtension {
        private final String fileName;
        private final String extension;

        private FileNameAndExtension(String fileName, String extension, boolean extensionToLowerCase) {
            this.fileName = fileName;
            this.extension = extensionToLowerCase ? extension.toLowerCase() : extension;
        }

        public String getFileName() {
            return fileName;
        }

        public String getExtension() {
            return extension;
        }

        @Override
        public String toString() {
            return extension == null ? fileName : fileName + "." + extension;
        }
    }

    /**
     * 获取文件的后缀名
     *
     * @param file 文件对象
     * @return 文件不存在或文件是目录时返回空字符串
     */
    public static String getFileExtension(File file) {
        String fileExtension = "";
        // 文件必须存在且不是目录
        if (null != file && file.exists() && !file.isDirectory()) {
            fileExtension = getFilextension(file.getName());
        }
        return fileExtension;
    }

    /**
     * 根据文件名获取文件的后缀
     *
     * @param fileName 文件名
     * @return 如果参数为空将返回空字符串
     */
    public static String getFilextension(String fileName) {
        String fileExtension = "";

        if (StringUtils.isNotBlank(fileName)) {
            fileExtension = StringUtils.substring(fileName, fileName.lastIndexOf(".") + 1, fileName.length());
        }

        return fileExtension;
    }

    /**
     * 获取classes的根路径
     * <li>应用容器中根路径为：{web}/WEB-INF/classes/
     * <li>测试环境中根路径为：{project}/target/test-classes/
     *
     * @return classes加载路径的根路径(绝对路径 ， 包含文件分隔符)
     */
    public static String getClassRootPath() {
        String absolutePath = FileUtils.class.getClassLoader().getResource("").getPath();
        return decode(absolutePath);
    }

    /**
     * 获取项目的根路径
     * <li>应用容器中返回值为：{web path}
     * <li>测试环境中根路径为：{project path}
     *
     * @return 项目根路径(不以文件分隔符结束)
     */
    public static String getProjectRootPath() {
        File classRootPath = new File(getClassRootPath());
        return classRootPath.getParentFile().getParentFile().getAbsolutePath();
    }

    /**
     * 获取clasz的绝对路径
     *
     * @param clasz
     * @return
     */
    public static String getClassPath(Class<?> clasz) {
        String classAbsolutePath = clasz.getResource("").getPath();
        return decode(classAbsolutePath);
    }

    /**
     * 文件路径中包括特殊字符时将导致路径访问失败，需要调用此方法进行路径解码
     *
     * @param filePath 文件路径
     * @return 解码后的路径
     */
    public static String decode(String filePath) {
        try {
            filePath = URLDecoder.decode(filePath, "utf-8");
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("对{}进行解码失败", e, filePath);
        }
        return filePath;
    }

    /**
     * 检测文件夹路径下文件是否存在,若不存在则创建文件夹
     *
     * @param path 文件夹路径
     * @return true 文件夹路径存在或创建成功
     */
    public static boolean checkAndCreateFolder(String path) {
        File file = new File(path);
        return checkAndCreateFolder(file);
    }

    /**
     * 检测文件夹是否存在,若不存在则创建文件夹
     *
     * @param file 文件夹对象
     * @return true 文件夹路径存在或创建成功
     */
    public static boolean checkAndCreateFolder(File file) {
        boolean flag = true;
        if (!file.exists()) {
            flag = file.mkdirs();
        }
        return flag;
    }

    /**
     * 检查文件路径下文件是否存在，若不存在创建该文件
     *
     * @param path 文件路径
     * @return true 文件路径下的文件存在或创建成功
     * @throws IOException
     */
    public static boolean checkAndCreateFile(String path) throws IOException {
        File file = new File(path);
        return checkAndCreateFile(file);
    }

    /**
     * 检查文件是否存在，若不存在则创建该文件
     *
     * @param file 文件对象
     * @return true 文件存在或创建成功
     * @throws IOException
     */
    public static boolean checkAndCreateFile(File file) throws IOException {
        boolean flag = true;
        if (!file.exists()) {
            File folder = file.getParentFile();
            if (!folder.exists()) {
                flag = folder.mkdirs();
            }
            if (flag) {
                flag = file.createNewFile();
            }
        }
        return flag;
    }

    public static String getMD5(File file) {
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(org.apache.commons.io.FileUtils.readFileToByteArray(file));
            BigInteger bigInt = new BigInteger(1, md5.digest());
            return bigInt.toString(16);
        } catch (NoSuchAlgorithmException | IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return StringUtils.EMPTY;
    }
}
