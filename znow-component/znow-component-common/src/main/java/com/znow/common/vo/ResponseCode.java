package com.znow.common.vo;

/**
 * TODO
 *
 * @author yuan 2018/10/11 15:07
 */
public enum ResponseCode {

    /**
     * 返回成功消息
     **/
    SUCCESS(200,"SUCCESS"),

    /** 
     * 返回错误消息
     **/
    ERROR(400,"ERROR");

    private final int code;

    private final String desc;

    ResponseCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public int getCode() {
        return code;
    }

}
