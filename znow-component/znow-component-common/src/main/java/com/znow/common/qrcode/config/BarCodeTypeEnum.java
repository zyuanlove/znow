package com.znow.common.qrcode.config;

import org.krysalis.barcode4j.impl.AbstractBarcodeBean;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.impl.code39.Code39Bean;
import org.krysalis.barcode4j.impl.upcean.EAN8Bean;
import org.krysalis.barcode4j.impl.upcean.UPCABean;

/**
 * 生成条形码类型枚举类
 *
 * @author yuan 2019/8/2 15:09
 */
public enum BarCodeTypeEnum {
    /**
     * Code128码（Code128码，包括EAN128码）
     */
    BARCODE128(new Code128Bean()),
    /**
     * Code39码（标准39码）
     */
    BARCODE39(new Code39Bean()),
    /**
     * EAN-8码（EAN-8国际商品条码）长度字符长度为8
     */
    EAN(new EAN8Bean()),

    UPCA(new UPCABean());

    private AbstractBarcodeBean object;

    private BarCodeTypeEnum(AbstractBarcodeBean object) {
        this.object = object;
    }

    public AbstractBarcodeBean getObject() {
        return object;
    }

    public void setObject(AbstractBarcodeBean object) {
        this.object = object;
    }

}
