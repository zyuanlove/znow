package com.znow.common.utils.snowflake;


/**
 * 基于Twitter的Snowflake算法实现分布式高效有序ID生产黑科技(sequence)
 */
public class SequenceUtils {

    private static long workerId = 3L;

    private static long dataCenterId = 4L;

    private static Sequence sequence = new Sequence(workerId, dataCenterId);

    public static Long nextLongId() {
        return sequence.nextId();
    }

    public static String nextStringId() {
        return String.valueOf(sequence.nextId());
    }

    public static void main(String[] args) {
        System.out.println(nextStringId());
    }

}