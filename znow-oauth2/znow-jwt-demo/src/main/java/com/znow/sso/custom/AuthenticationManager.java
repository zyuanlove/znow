package com.znow.sso.custom;

public interface AuthenticationManager {

    /**
     * 尝试对传递的{@link Authentication}对象进行身份验证，如果成功，则返回*完全填充的<code> Authentication </ code>对象（包括授予的权限）。
     * 必须遵守以下有关例外的约定：
     * <ul>
     * <li>如果禁用了帐户并且* <code，必须抛出{@link DisabledException} > AuthenticationManager </ code>可以测试此状态。</li>
     * <li>如果帐户被锁定并且* <code> AuthenticationManager </ code>可以测试帐户锁定，则必须抛出{@link LockedException} </li>
     * <li>如果提供的凭据不正确，则必须抛出{@link BadCredentialsException}。尽管上述异常是可选的，</li>
     * </ ul>
     * 应该对异常进行测试，如果适用，则将其抛出上面表示的顺序（即，如果帐户被禁用或锁定，则身份验证请求将立即被拒绝*并且不执行凭据测试过程）。
     * *防止针对禁用或锁定的帐户测试凭据。
     *
     * @param 验证身份验证请求对象
     * @return 包含证书的完全验证对象
     * @throws 验证失败则抛出 AuthenticationException
     */
    void authenticate(Authentication authentication);
}
