package com.znow.sso.dto;

import lombok.Data;

@Data
public class LoginDto {

    private Integer userId;

    private String username;
}
