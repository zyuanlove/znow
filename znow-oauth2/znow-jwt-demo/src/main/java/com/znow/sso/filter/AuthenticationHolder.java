package com.znow.sso.filter;

import org.springframework.util.Assert;

/**
 * 全局对象
 *
 * @author yuan 2020/4/22 15:32
 */
public class AuthenticationHolder {

    private final static ThreadLocal userThreadLocal = new ThreadLocal();

    private AuthenticationHolder() {
    }

    public static void clearContent() {
        userThreadLocal.remove();
    }

    public static Object getContent(){
        return userThreadLocal.get();
    }

    public static void setContent(Object obj) {
        Assert.notNull(obj, "对象为空！");
        userThreadLocal.set(obj);
    }
}
