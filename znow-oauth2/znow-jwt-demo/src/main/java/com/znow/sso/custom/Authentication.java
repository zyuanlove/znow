package com.znow.sso.custom;

import java.io.Serializable;
import java.security.Principal;
import java.util.List;

public interface Authentication extends Principal, Serializable {

    /**
     * 授权相关的权限信息
     */
    List getAuthorities();

    /**
     * 密码凭据
     */
    Object getCredentials();

    /**
     * 存储有关身份验证请求的其他详细信息。这些可能是IP地址，证书序列号等。* *
     *
     * @retrun 有关身份验证请求的其他详细信息，或者 <code> null </code> *（如果未使用）
     */
    Object getDetails();

    /**
     * 身份验证的主体的身份。如果使用用户名和密码进行身份验证*请求，则为用户名。 *要求呼叫者填充身份验证请求的主体。
     * <p>其中包含更丰富的信息作为供应用程序使用的主体。许多身份验证提供程序将创建一个* {@code UserDetails}对象作为主体。</p>
     */
    Object getPrincipal();

    /**
     * 是否认证成功
     */
    boolean isAuthenticated();

    /**
     * See {@link #isAuthenticated()} for a full description.
     * <p>
     * Implementations should <b>always</b> allow this method to be called with a
     * <code>false</code> parameter, as this is used by various classes to specify the
     * authentication token should not be trusted. If an implementation wishes to reject
     * an invocation with a <code>true</code> parameter (which would indicate the
     * authentication token is trusted - a potential security risk) the implementation
     * should throw an {@link IllegalArgumentException}.
     *
     * @param isAuthenticated <code>true</code> if the token should be trusted (which may
     *                        result in an exception) or <code>false</code> if the token should not be trusted
     * @throws IllegalArgumentException if an attempt to make the authentication token
     *                                  trusted (by passing <code>true</code> as the argument) is rejected due to the
     *                                  implementation being immutable or implementing its own alternative approach to
     *                                  {@link #isAuthenticated()}
     */
    void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException;
}
