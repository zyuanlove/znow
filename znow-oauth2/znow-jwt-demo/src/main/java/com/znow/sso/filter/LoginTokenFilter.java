package com.znow.sso.filter;

import com.znow.sso.dto.LoginDto;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class LoginTokenFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        System.out.println("登录过滤器");
        LoginDto loginDto = new LoginDto();
        loginDto.setUserId(1);
        loginDto.setUsername("zhangyuan");
        AuthenticationHolder.setContent(loginDto);
        filterChain.doFilter(request,response);
    }

}
