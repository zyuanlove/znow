package com.znow.sso.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

@Component
public class JwtTokenUtils {

    /**
     * 签名秘钥
     */
    private String secret = "25f9e794323b453885f5181f1b624d0b";

    /**
     * 创建token时间
     */
    private static final String CLAIM_CREATE_TIME = "created";

    /**
     * 判断令牌是否过期
     *
     * @param token 令牌
     * @return 是否过期
     */
    public boolean isTokenExpired(String token) {
        try {
            Claims claims = getClaimsToken(token);
            Date expiration = claims.getExpiration();
            return expiration.before(new Date());
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 从令牌中获取数据声明
     *
     * @param token 令牌
     * @return 数据声明
     */
    private Claims getClaimsToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
        } catch (Exception e) {
            claims = null;
        }
        return claims;
    }

    /**
     * <li>iss: 该JWT的签发者</li>
     * <li>sub: 该JWT所面向的用户</li>
     * <li>aud: 接收该JWT的一方</li>
     * <li>exp(expires): 什么时候过期，这里是一个Unix时间戳</li>
     * <li>iat(issued at): 在什么时候签发的</li>
     */
    public String createJwtToken(Map<String, Object> claims, Long expireTime) {
        return Jwts.builder()
                // 创建payload的私有声明
                .addClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secret)
                .setExpiration(this.generateExpirationDate(expireTime)).compact();
    }

    /**
     * 根据颁发的refreshToken刷新accessToken
     *
     * @param token accessToken
     * @return 刷新后的新accessToken
     */
    private String refreshToken(String token, Long expireTime) {
        String refreshedToken = null;
        try {
            final Claims claims = getClaimsToken(token);
            if (null != claims) {
                claims.put(CLAIM_CREATE_TIME, new Date());
                refreshedToken = createJwtToken(claims, expireTime);
            }
        } catch (Exception e) {
            refreshedToken = null;
        }
        return refreshedToken;
    }

    /**
     * 创建token过期时间
     */
    private Date generateExpirationDate(Long expireTime) {
        if (expireTime == null || expireTime <= 0) {
            throw new IllegalArgumentException("过期时间为空！");
        }
        return new Date(System.currentTimeMillis() + expireTime * 1000L);
    }

    /**
     * 获取request请求头的token值
     *
     * @param request
     * @return token
     **/
    public String getHeaderAuthToken(HttpServletRequest request) {
        String authToken = null;
        String authHeader = request.getHeader("Authorization");
        String tokenHead = "Bearer";
        if (authHeader != null && authHeader.startsWith(tokenHead)) {
            authToken = authHeader.substring(tokenHead.length());
        }
        return authToken;
    }
}
