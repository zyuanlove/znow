package com.znow.sso.custom;

import java.util.Collections;
import java.util.List;

public class ProviderManager implements AuthenticationManager{

    /**
     * 认证提供者列表
     */
    private List<AuthenticationProvider> providers = Collections.emptyList();

    public ProviderManager(List<AuthenticationProvider> providers) {
        this.providers = providers;
    }

    public List<AuthenticationProvider> getProviders() {
        return providers;
    }

    @Override
    public void authenticate(Authentication authentication) {
        Class<? extends Authentication> toTest = authentication.getClass();
        for (AuthenticationProvider provider : providers) {
            if(!provider.supports(toTest)){
                continue;
            }
        }
    }
}
