package com.znow.sso.controller;

import com.google.common.collect.Maps;
import com.znow.common.utils.FileUtils;
import com.znow.sso.dto.LoginDto;
import com.znow.sso.filter.AuthenticationHolder;
import com.znow.sso.utils.JwtTokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class LoginController {

    @Autowired
    private JwtTokenUtils jwtTokenUtils;

    @RequestMapping("/login")
    public String login(String username) throws InterruptedException {
        LoginDto loginDto = new LoginDto();
        loginDto.setUsername(username);
        Map loginMap = Maps.newConcurrentMap();
        loginMap.put("user",loginDto);
        loginMap.put("created",new Date());
        System.out.println("执行完业务逻辑");
        Thread.sleep(3000L);
        return jwtTokenUtils.createJwtToken(loginMap,60 * 30L);
        //System.out.println(AuthenticationHolder.getRequestIp());
    }
}
