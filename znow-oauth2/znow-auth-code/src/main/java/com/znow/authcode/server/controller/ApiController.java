package com.znow.authcode.server.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ApiController {

    @RequestMapping("/userInfo")
    public ResponseEntity getUserInfo() {
        return ResponseEntity.ok().body(SecurityContextHolder.getContext()
                .getAuthentication());
    }

}
