package com.znow.social.commpent;

import com.znow.common.utils.httpclient.HttpClientUtils;
import com.znow.social.AbstractSocialAuthTemplate;
import com.znow.social.AuthPathSourceEnum;
import com.znow.social.config.AuthAccountConfig;
import com.znow.social.response.AuthTokenResponse;

import java.util.Map;

public class WechatMinProgramAuthRequest extends AbstractSocialAuthTemplate {

    public WechatMinProgramAuthRequest(AuthAccountConfig config,
                                       AuthPathSourceEnum authPathSourceEnum,
                                       HttpClientUtils httpClientUtils) {
        super(config, authPathSourceEnum, httpClientUtils);
    }

    @Override
    protected AuthTokenResponse getAccessToken() {
        return null;
    }

    @Override
    protected String authorize() {
        return null;
    }

    @Override
    protected String getAccessTokenUrl(String code) {
        Map<String, Object> params = createHashMap();
        params.put("code", code);
        params.put("client_id", config.getClientSecret());
        params.put("client_secret", config.getClientSecret());
        params.put("grant_type", "authorization_code");
        params.put("redirect_uri", config.getRedirectUri());
        return buildAuthUrl(authPathSourceEnum.accessToken(), params);
    }
}
