package com.znow.social;

public enum AuthPathSourceEnum implements AuthPathSource {

    /**
     * 微信公众平台
     */
    WECAT_OPEN {
        @Override
        public String authorize() {
            return "https://open.weixin.qq.com/connect/qrconnect";
        }

        @Override
        public String accessToken() {
            return "https://api.weixin.qq.com/sns/oauth2/access_token";
        }

        @Override
        public String userInfo() {
            return "https://api.weixin.qq.com/sns/userinfo";
        }

        @Override
        public String refreshToken() {
            return "https://api.weixin.qq.com/sns/oauth2/refresh_token";
        }
    },

    WECAT_MINIPROGRAM{
        @Override
        public String authorize() {
            return "https://api.weixin.qq.com/sns/jscode2session";
        }

        @Override
        public String accessToken() {
            return "https://api.weixin.qq.com/cgi-bin/token";
        }

        @Override
        public String userInfo() {
            return "https://api.weixin.qq.com/sns/userinfo";
        }

        @Override
        public String refreshToken() {
            return "https://api.weixin.qq.com/sns/oauth2/refresh_token";
        }
    }
}
