package com.znow.social.commpent;

import com.znow.common.utils.httpclient.HttpClientConfig;
import com.znow.common.utils.httpclient.HttpClientUtils;
import com.znow.social.AbstractSocialAuthTemplate;
import com.znow.social.AuthPathSourceEnum;
import com.znow.social.config.AuthAccountConfig;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;

public class Test {

    public static void main(String[] args) {

        AuthAccountConfig authAccountConfig = AuthAccountConfig.builder()
                .build();

        CloseableHttpClient closeableHttpClient = new HttpClientConfig().httpClient();
        RequestConfig requestConfig = new HttpClientConfig().createRequestConfig();
        HttpClientUtils httpClientUtils = new HttpClientUtils(closeableHttpClient, requestConfig);

        WechatMinProgramAuthRequest wechatMinProgramAuthRequest = new WechatMinProgramAuthRequest(
                authAccountConfig, AuthPathSourceEnum.WECAT_OPEN,httpClientUtils);

        System.out.println(wechatMinProgramAuthRequest.getAccessTokenUrl("123"));
    }
}
