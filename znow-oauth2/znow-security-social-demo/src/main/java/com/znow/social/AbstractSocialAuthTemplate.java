package com.znow.social;

import com.znow.common.utils.httpclient.HttpClientUtils;
import com.znow.social.config.AuthAccountConfig;
import com.znow.social.response.AuthTokenResponse;
import org.apache.http.client.utils.URIBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * 接入第三方登录通用抽象类
 *
 * @author yuan 2020/4/28 16:49
 */
public abstract class AbstractSocialAuthTemplate {

    protected AuthAccountConfig config;

    protected AuthPathSourceEnum authPathSourceEnum;

    protected HttpClientUtils httpClientUtils;

    public AbstractSocialAuthTemplate(AuthAccountConfig config,
                                      AuthPathSourceEnum authPathSourceEnum,
                                      HttpClientUtils httpClientUtils) {
        this.config = config;
        this.authPathSourceEnum = authPathSourceEnum;
        this.httpClientUtils = httpClientUtils;
    }

    /**
     * 获取access token
     *
     * @return token
     */
    protected abstract AuthTokenResponse getAccessToken();

    /**
     * 获取access token
     *
     * @return token
     */
    protected abstract String authorize();

    protected abstract String getAccessTokenUrl(String code);

    protected String authorizeUrl(String state) {
        Map<String, Object> params = createHashMap();
        params.put("response_type", "code");
        params.put("client_id", config.getClientId());
        params.put("client_secret", config.getClientSecret());
        params.put("redirect_uri", "authorization_code");
        params.put("state", "");
        return buildAuthUrl(authPathSourceEnum.accessToken(), params);
    }

    protected String doAuthorizationCode(String code) {
        return httpClientUtils.doGet(getAccessTokenUrl(code));
    }

    /**
     * 构建请求路径
     */
    protected static String buildAuthUrl(String baseAuthUrl, Map<String, Object> parameters) {
        try {
            URIBuilder uriBuilder = new URIBuilder(baseAuthUrl);
            if (!parameters.isEmpty()) {
                for (Map.Entry<String, Object> entry : parameters.entrySet()) {
                    String name = formEncode(entry.getKey());
                    String value = String.valueOf(entry.getValue());
                    uriBuilder.addParameter(name, value);
                }
            }
            return uriBuilder.build().toString();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String formEncode(String data) {
        try {
            return URLEncoder.encode(data, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            throw new IllegalStateException(ex);
        }
    }

    /**
     * 创建一个HashMap对象
     *
     * @return hashMap
     */
    public static <K, V> HashMap<K, V> createHashMap() {
        return new HashMap<>();
    }
}
