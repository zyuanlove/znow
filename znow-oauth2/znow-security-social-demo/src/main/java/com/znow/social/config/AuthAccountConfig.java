package com.znow.social.config;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * 第三方登录授权账号配置
 *
 * @author yuan 2020/5/9 16:32
 */
@Setter
@Getter
@Builder
public class AuthAccountConfig {

    private String clientId;

    private String clientSecret;

    private String redirectUri;
}
