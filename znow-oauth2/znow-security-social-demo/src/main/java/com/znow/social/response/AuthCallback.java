package com.znow.social.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 授权回调时的参数类
 *
 */
@Getter
@Setter
@Builder
public class AuthCallback implements Serializable {

    private static final long serialVersionUID = 4534257824968378635L;

    /**
     * 访问AuthorizeUrl后回调时带的参数code
     */
    private String authCode;

    /**
     * 访问AuthorizeUrl后回调时带的参数state，用于和请求AuthorizeUrl前的state比较，防止CSRF攻击
     */
    private String state;

    /**
     * 回调后返回的oauth_token
     *
     * @since 1.13.0
     */
    private String oauthToken;
}
