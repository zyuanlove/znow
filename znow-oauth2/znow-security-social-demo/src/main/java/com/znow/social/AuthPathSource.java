package com.znow.social;

/**
 * oauth2授权路径
 */
public interface AuthPathSource {

    /**
     * 授权的api
     *
     * @return url
     */
    String authorize();

    /**
     * 获取accessToken的api
     *
     * @return url
     */
    String accessToken();

    /**
     * 获取用户信息的api
     *
     * @return url
     */
    String userInfo();

    /**
     * 刷新token路径
     */
    String refreshToken();
}
