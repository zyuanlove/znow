package com.znow.social.commpent;


import com.znow.common.utils.httpclient.HttpClientUtils;
import com.znow.social.AbstractSocialAuthTemplate;
import com.znow.social.AuthPathSourceEnum;
import com.znow.social.config.AuthAccountConfig;
import com.znow.social.response.AuthTokenResponse;

public class WechatOpenAuthRequest extends AbstractSocialAuthTemplate {

    public WechatOpenAuthRequest(AuthAccountConfig config,
                                 AuthPathSourceEnum authPathSourceEnum,
                                 HttpClientUtils httpClientUtils) {
        super(config, authPathSourceEnum, httpClientUtils);
    }

    @Override
    protected AuthTokenResponse getAccessToken() {
        return null;
    }

    @Override
    protected String authorize() {
        return null;
    }

    @Override
    protected String getAccessTokenUrl(String code) {
        return null;
    }
}
