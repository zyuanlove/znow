package com.znow.social.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * 第三方授权通过code获取access_token返回实体
 *
 * @author yuan 2020/4/29 10:05
 */
@Setter
@Getter
@Builder
public class AuthTokenResponse {

    private String uid;

    /**
     * 第三方授权码接口调用凭证
     */
    private String accessToken;

    /**
     * access_token接口调用凭证超时时间，单位（秒）
     */
    private Integer expireIn;

    /**
     * 刷新access_token
     */
    private String refreshToken;

    /**
     * 授权用户唯一标识
     */
    private String openId;

    /**
     * 接口调用凭证码
     */
    private String accessCode;

    /**
     * unionid来区分用户的唯一性
     */
    private String unionId;
}
