package com.znow.order.web.controller;

import com.znow.common.vo.ServerResponse;
import com.znow.order.server.feign.IProductFeign;
import com.znow.order.server.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HandlerAdapter;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private IOrderService orderService;

   /* @Autowired
    private IProductFeign iProductFeign;*/

    @RequestMapping("/create")
    public ServerResponse createOrder(){
        //HandlerAdapter

        //System.out.println(iProductFeign.getList());
        return orderService.createOrder();
    }

    @RequestMapping("/hello")
    public String helloWord(){
       return "helloWorld";
    }
}
