package com.znow.order.server.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value = "znow-com.znow.resource-service",path = "/com.znow.resource")
public interface IProductFeign {

    @RequestMapping("/list")
    String getList();
}
