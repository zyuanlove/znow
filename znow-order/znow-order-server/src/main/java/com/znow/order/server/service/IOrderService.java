package com.znow.order.server.service;

import com.znow.common.vo.ServerResponse;

public interface IOrderService {

    ServerResponse createOrder();
}
