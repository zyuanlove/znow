package com.znow.order.server.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.SingleServerConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Redission配置类
 *
 * @author yuan 2019/7/10 17:29
 */
@Configuration
public class RedissonConfig {

    @Value("${spring.redis.host}")
    private String redisHost;

    @Value("${spring.redis.port}")
    private String redisPort;

    @Bean
    public RedissonClient redissonClient() {
        Config config = new Config();
        // SentinelServersConfig 哨兵模式
        // 单机模式
        SingleServerConfig serverConfig = config.useSingleServer();
        serverConfig.setAddress("redis://" + redisHost + ":" + redisPort);
        serverConfig.setTimeout(3000);
        serverConfig.setConnectionPoolSize(64);
        serverConfig.setConnectionMinimumIdleSize(24);
        //serverConfig.setPassword("");
        return Redisson.create(config);
    }

    
}
