package com.znow.order.server.service.impl;

import com.znow.common.utils.UUIDUtils;
import com.znow.common.vo.ServerResponse;
import com.znow.order.server.config.RedissonUtils;
import com.znow.order.server.service.IOrderService;
import org.redisson.api.RLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.BeanDefinitionReader;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

@Service
public class OrderServiceImpl implements IOrderService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private RedissonUtils redissonUtils;

    public static final String ORDER_KEY = "order:stock";

    @PostConstruct
    public void init() {
        redisTemplate.opsForValue().set(ORDER_KEY, "1000");
    }

    @Override
    public ServerResponse createOrder() {
        boolean lock = false;
        try{
            lock = redissonUtils.tryLock("order:lock", TimeUnit.SECONDS, 3, 10);
            if(lock){
                String stock = redisTemplate.opsForValue().get(ORDER_KEY);
                if (stock != null) {
                    Integer stock2 = Integer.valueOf(stock);
                    redisTemplate.opsForValue().set(ORDER_KEY, (stock2 - 1) + "");
                }
            }
        }catch (Exception e){

        }finally {
            if(lock){
                redissonUtils.unlock("order:lock");
            }
        }
        return ServerResponse.createBySuccessMsg(redisTemplate.opsForValue().get(ORDER_KEY));
    }

}
