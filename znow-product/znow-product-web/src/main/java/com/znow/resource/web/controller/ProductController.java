package com.znow.resource.web.controller;

import com.znow.resource.server.dto.GoodsDto;
import com.znow.resource.server.entity.GoodsBean;
import com.znow.resource.server.service.GoodsService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/com.znow.resource")
public class ProductController {

    @Autowired
    private GoodsService goodsService;

    @RequestMapping("/list")
    public String queryByList() {
        return "商品服务列表";
    }

    @RequestMapping("/update")
    public String update(GoodsDto goodsDto) {
        GoodsBean goodsBean = new GoodsBean();
        BeanUtils.copyProperties(goodsDto, goodsBean);
        int result = goodsService.updateGoods(goodsBean);
        if (result > 0) {
            return "更新成功";
        } else {
            return "更新失败";
        }
    }

    @RequestMapping("/add")
    public String add(GoodsDto goodsDto) {
        GoodsBean goodsBean = new GoodsBean();
        BeanUtils.copyProperties(goodsDto, goodsBean);
        int result = goodsService.insertGoods(goodsBean);
        if (result > 0) {
            return "更新成功";
        } else {
            return "更新失败";
        }
    }
}
