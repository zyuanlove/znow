package com.znow.resource.server.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class GoodsDto implements Serializable {
    /**
     * 商品表主键
     */
    private Long goodsId;

    /**
     * 商品分类id
     */
    private Long categoryId;

    /**
     * 品牌表
     */
    private Long brandId;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品副标题
     */
    private String subTitle;

    /**
     * 商品价格
     */
    private BigDecimal price;

    /**
     * 商品主图
     */
    private String mainImage;

    /**
     * 商品状态(PUBLISHED：上架状态；UN_PUBLISH：未上架)
     */
    private String status;

    /**
     * 
     */
    private Integer goodsSort;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 
     */
    private Integer version;

    /**
     * 商品图片，以逗号分隔
     */
    private String subImages;

    /**
     * 商品描述
     */
    private String description;

}