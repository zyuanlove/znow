package com.znow.resource.server.service.impl;

import com.znow.resource.server.dao.GoodsMapper;
import com.znow.resource.server.entity.GoodsBean;
import com.znow.resource.server.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsMapper goodsMapper;

    @Override
    public int updateGoods(GoodsBean goodsBean) {
        return goodsMapper.updateByPrimaryKeySelective(goodsBean);
    }

    @Override
    public int insertGoods(GoodsBean goodsBean) {
        return goodsMapper.insert(goodsBean);
    }

}
