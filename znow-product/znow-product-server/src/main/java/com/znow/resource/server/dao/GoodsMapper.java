package com.znow.resource.server.dao;

import com.znow.resource.server.entity.GoodsBean;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GoodsMapper {
    /**
     * 根据ID删除
     *
     * @param goodsId 主键ID
     */
    int deleteByPrimaryKey(Long goodsId);

    /**
     * 添加对象所有字段
     *
     * @param goodsBean 插入字段对象(必须含ID）
     */
    int insert(GoodsBean goodsBean);

    /**
     * 根据ID查询
     *
     * @param goodsId 主键ID
     */
    GoodsBean selectByPrimaryKey(Long goodsId);

    /**
     * 根据ID修改对应字段
     *
     * @param goodsBean 修改字段对象(必须含ID）
     */
    int updateByPrimaryKeySelective(GoodsBean goodsBean);
}