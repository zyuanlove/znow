package com.znow.resource.server.service;

import com.znow.resource.server.entity.GoodsBean;

public interface GoodsService {

    int updateGoods(GoodsBean goodsBean);

    int insertGoods(GoodsBean goodsBean);
}
